// Fill out your copyright notice in the Description page of Project Settings.

#include "ActionHandler.h"
#include <Containers/Queue.h>
#include "CommandBNF/Actions/ActionVisitorImpl.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/GameEngine.h"
#include "Character/SpyController.h"
#include "GameFramework/Character.h"
#include "CommandBNF/Locations/AbsoluteLocation.h"
#include "CommandBNF/Locations/ObjectRelativeLocation.h"
#include "CommandBNF/Locations/CharacterRelativeLocation.h"
#include "CommandBNF/Actions/SnapRotation.h"
#include "Extras/Either.h"
#include "Extras/MoveData.h"
#include "Extras/CompositeData.h"
#include "CommandBNF/Parsing/json_library.hpp"
#include "Engine/World.h"
#include <vector>
#include <functional>


AActionHandler::AActionHandler()
{
	UE_LOG(LogTemp, Warning, TEXT("ActionHandler: Instantiated"));
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void AActionHandler::BeginPlay() 
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("ActionHandler: BeginPlay"));
	Tags.Add(FName("ActionHandler"));
}

#include "CommandBNF/Parsing/json_library.hpp"
using nlohmann::json;
#include "CommandBNF/Parsing/action_parser.hpp"
using namespace json_parsing;

#include "Response.h"
#include "CommandBNF/Actions/ChangeStance.h"
#include "CommandBNF/Actions/ChangeSpeed.h"
#include "CommandBNF/Actions/Movement.h"
#include "CommandBNF/Locations/EscapeRelativeLocation.h"
void AActionHandler::ExecuteJSONAsAction(UConnection* connection) {
	is_expecting_response = true;
	current_connection = connection;

	FString actionJSONFString = connection->GetData();
	FString contentLengthFString = connection->GetHeader(FString("Content-Length"));
	UE_LOG(LogTemp, Warning, TEXT("Server Command"));
	UE_LOG(LogTemp, Warning, TEXT("string - %s"), *actionJSONFString);
	int contentLength = std::stoi(std::string(TCHAR_TO_UTF8(*contentLengthFString)));
	std::string actionJSONString = std::string(TCHAR_TO_UTF8(*actionJSONFString)).substr(0, contentLength);
	UE_LOG(LogTemp, Warning, TEXT("string - %s"), *FString(actionJSONString.c_str()));
	json actionJSON = json::parse(actionJSONString);
	Action* nextAction = parse_action(actionJSON);
	if (nextAction == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Action Response - Action Invalid"));
		std::string errorMessage = "Action invalid";
		std::string failureResponse = "{ \"type\":\"failure\", \"description\":\"" + errorMessage + "\"}";
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(failureResponse.c_str()));
		connection->SendResponse(response);
	}
	else {
		if (nextAction->type == Action::ActionType::CHANGE_STANCE) {
			if (currentAction != nullptr && currentAction->type == Action::ActionType::MOVEMENT) {
				ChangeStance* changeStance = static_cast<ChangeStance*>(nextAction);
				Movement* movement = static_cast<Movement*>(currentAction);
				movement->stance = changeStance->stance;
				currentAction = movement;
			}
			else currentAction = nextAction;
		}
		else if (nextAction->type == Action::ActionType::CHANGE_SPEED) {
			if (currentAction != nullptr && currentAction->type == Action::ActionType::MOVEMENT) {
				ChangeSpeed* changeSpeed = static_cast<ChangeSpeed*>(nextAction);
				Movement* movement = static_cast<Movement*>(currentAction);
				movement->speed = changeSpeed->speed;
				currentAction = movement;
			}
			else currentAction = new Movement(GameObject("guard", new EscapeRelativeLocation()), Speed::RUN, Stance::STAND, true);
		}
		else currentAction = nextAction;

		queueAction(currentAction);
	}
}

void AActionHandler::queueAction(Action* action) {
	if (action == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Trying to queue nullptr Action*"));
		return;
	}
	if (action->type == Action::ActionType::COMPOSITE) {
		CompositeAction* composite = static_cast<CompositeAction*>(action);
		TQueueDelayedActions.Empty();
		TQueueDelayedActions.Enqueue(new Stop(false));
		for (Action* subMove : composite->actions) TQueueDelayedActions.Enqueue(subMove);
		TQueueDelayedActions.Enqueue(new SnapRotation(SnapRotation::Direction::NONE, false));
		nextAction();
	}
	else {
		TQueueDelayedActions.Empty();
		TQueueDelayedActions.Enqueue(new Stop(false));
		TQueueDelayedActions.Enqueue(action);
		TQueueDelayedActions.Enqueue(new SnapRotation(SnapRotation::Direction::NONE, false));
		nextAction();
	}
}


void AActionHandler::EvaluateAction(Action* action)
{
	UE_LOG(LogTemp, Warning, TEXT("Evaluate Action"));

	AActionVisitorImpl* action_visitor = Cast<AActionVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), AActionVisitorImpl::StaticClass(), FTransform()));
	action_visitor->Init(this);
	UGameplayStatics::FinishSpawningActor(action_visitor, FTransform());

	std::function<void(Action*, Either<CompositeData*, ActionError>)> HandleEvaluatedAction = 
			[&](Action* action, Either<CompositeData*, ActionError> result) -> void {
		UE_LOG(LogTemp, Warning, TEXT("Handle Evaluated Action"));
		FString responseMessage;
		if (result.getType() == Type::LEFT){
			CompositeData* subMoves = result.getLeft();
			spyController->QueueAction(subMoves);
			json successResponse;
			successResponse["type"] = "success";
			responseMessage = FString(successResponse.dump().c_str());
		}
		else {
			ActionError actionError = result.getRight();
			ActionError::ActionErrorCode errorCode = actionError.errorCode;
			std::string errorSubject = actionError.subject;
			//UE_LOG(LogTemp, Warning, TEXT("Error:<%s>"), *FString(errorMessage.c_str()));
			json failureResponse;
			failureResponse["type"] = "failure";
			failureResponse["error_code"] = errorCode;
			failureResponse["subject"] = errorSubject;
			responseMessage = FString(failureResponse.dump().c_str());
		}
		if (action->isRequestedAction) {
			//Respond to current client request with first (non-stop) evaluated action
			UResponse* response = UResponse::ConstructResponseExt(GetWorld(), responseMessage);
			current_connection->SendResponse(response);
			//Disables composite movements from respond to client multiple times
			UE_LOG(LogTemp, Warning, TEXT("SENDING-JSON[%s]"), *responseMessage);
		}
	};

	action->accept(action_visitor, HandleEvaluatedAction);
}


void AActionHandler::nextAction()
{
	if (!TQueueDelayedActions.IsEmpty()) {
		Action* nextAction;
		TQueueDelayedActions.Dequeue(nextAction);
		EvaluateAction(nextAction);
	}
}

void AActionHandler::InitController(ASpyController* spyController)
{
	UE_LOG(LogTemp, Warning, TEXT("ActionHandler: InitController Set ASpyController"));
	this->spyController = spyController;
}

void AActionHandler::QueueStop()
{
	Action* stop = new Stop(false);
	queueAction(stop);
	if (currentAction->type == Action::ActionType::STRANGLE) {
		
	}
}

#include "CommandBNF/Actions/Fight.h"
void AActionHandler::QueueConditionalAttack()
{
	if (currentAction != nullptr && (currentAction->type == Action::ActionType::STRANGLE || currentAction->type == Action::ActionType::ATTACK)) {
		Action* fight = new Fight(GameObject("guard", new ObjectRelativeLocation()), false);
		queueAction(fight);
	}
}

#include "CommandBNF/Actions/Speed.h"
#include "CommandBNF/Actions/OpenDoor.h"
#include "CommandBNF/Actions/Knockout.h"
#include "CommandBNF/Actions/Throw.h"
#include "CommandBNF/Actions/Attack.h"
#include "CommandBNF/Actions/CompositeAction.h"
#include "CommandBNF/Actions/HackCamera.h"
#include "CommandBNF/Actions/HackTerminal.h"
void AActionHandler::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if (requestMove) {
		UE_LOG(LogTemp, Warning, TEXT("AActionHandler: Request Move Tick"));

		//Absolute Locations Tests
	
		//Action* stairsMovement = new StairsMovement(GameObject("stairs", new ObjectRelativeLocation()), FString("up"));
		//queueAction(stairsMovement);

		//Action* walkForwards = new Movement(GameObject(new CharacterRelativeLocation(Direction::INFRONT)), Speed::WALK, Stance::NO_CHANGE);
		//Action* strangle = new Strangle(GameObject("guard", new ObjectRelativeLocation()));
		//Action* composite = new CompositeAction({ strangle, walkForwards });
		//queueAction(composite);
		//queueAction(walkForwards);

		//Action* turn = new Turn(Turn::Direction::RIGHT);
		//Action* crouch = new ChangeStance(Stance::CROUCH);
		//Action* crouchRotation = new CompositeAction({ crouch,  turn });
		//queueAction(crouchRotation);

		//Action* action1 = new Movement(GameObject("lab 300", new AbsoluteLocation()), Speed::WALK, Stance::STAND);
		//queueAction(action1); //walk to the toilet
		//Action* action1 = new Pickup(GameObject("rock", new ObjectRelativeLocation()));
		//queueAction(action1); //pickup the rock
		
		//Action* action3 = new HackTerminal(GameObject("terminal", new ObjectRelativeLocation()));
		//queueAction(action3); //walk to the toilet

		//Action* action = new Drop();
		//queueAction(action);

		//Action* action1 = new Hide(GameObject("lab 300", new AbsoluteLocation()));
		//queueAction(action1); //hide in lab 300
		//Action* action1 = new Attack(GameObject("gaurd", new ObjectRelativeLocation()));
		//queueAction(action1);
		//Action* action1 = new Knockout(GameObject("gaurd", new ObjectRelativeLocation()));
		//queueAction(action1);
		//Action* action1 = new Movement(GameObject("lab 300", new AbsoluteLocation()), Speed::WALK, Stance::STAND);
		//queueAction(action1); //walk to the toilets
		//Action* action5 = new Movement(GameObject("male toilet 2", new AbsoluteLocation()), Speed::RUN, Stance::CROUCH);
		//HandleAction(action5); //crouch run to the toilet
		//Action* action6 = new Movement(GameObject("server room 2", new AbsoluteLocation()), Speed::WALK, Stance::CROUCH);
		//HandleAction(action6); //crouch to the toilet

		//Object Relative Locations Tests
		//Action* action2 = new Movement(GameObject("rock", new ObjectRelativeLocation()), Speed::WALK, Stance::STAND);
		//HandleAction(action2); //walk to the rock around you
		//Action* action2 = new Movement(GameObject("rock", new ObjectRelativeLocation(Direction::BEHIND)), Speed::WALK, Stance::STAND);
		//HandleAction(action2); //walk to the rock behind you
		//Action* action2 = new Movement(GameObject("roof", new ObjectRelativeLocation()), Speed::WALK, Stance::STAND);
		//queueAction(action2); //walk to the second rock to your left

		//Character Relative Locations Tests
		//Action* action1 = new Movement(GameObject(new CharacterRelativeLocation(Direction::INFRONT, 10)), Speed::WALK, Stance::STAND);
		//HandleAction(action1); //walk 10 steps in front of you (test that you cannot move through objects/walls or dest point isn't outside nav mesh)
		//Action* action2 = new Movement(GameObject(new CharacterRelativeLocation(Direction::INFRONT, 10)), Speed::WALK, Stance::CROUCH);
		//HandleAction(action2); //crouch 10 steps in front of you
		//Action* action1 = new Movement(GameObject(new CharacterRelativeLocation(Direction::LEFT, 10)), Speed::WALK, Stance::STAND);
		//HandleAction(action1); //walk 10 steps to the left of you 

		//Change Positions Tests
		//Action* action1 = new ChangeStance(Stance::CROUCH);
		//HandleAction(action1); // crouch
		//Action* action1 = new ChangeStance(Stance::STAND);
		//HandleAction(action1); //stand

		//Open Door Tests
		//Action* action2 = new OpenDoor(GameObject("door", new ObjectRelativeLocation()));
		//queueAction(action2); //open the closest door


		//Pickup Tests
		//Action* action1 = new Pickup(GameObject("rock", new ObjectRelativeLocation()));
		//queueAction(action1); //pickup the rock
		//Action* action1 = new Pickup(GameObject("rock", new ObjectRelativeLocation(Direction::LEFT)));
		//queueAction(action1); //pickup the rock infront of you


		//Throw Tests
		//Action* action1 = new Throw(GameObject(new CharacterRelativeLocation(Direction::INFRONT, 10)));
		//HandleAction(action1); //throw the object infront of you by a default amount
		//Action* action1 = new Throw(GameObject(new CharacterRelativeLocation(Direction::LEFT, 10)));
		//HandleAction(action1); //throw the object left of you by a default amount
		//Action* action1 = new Throw(GameObject("door", new ObjectRelativeLocation()));
		//HandleAction(action1); //throw the object at the door













		/*Action* action1 = new Movement(GameObject(new CharacterRelativeLocation(Direction::BEHIND, 10)), Speed::WALK, Stance::NO_CHANGE);
		HandleAction(action1);*/
		//Action* action1 = new Movement(GameObject("Male Toilet 2", new AbsoluteLocation()), Speed::RUN, Stance::STAND);
		//HandleAction(action1);
		//Action* action2 = new Movement(GameObject("Server Room 22", new AbsoluteLocation()), Speed::WALK, Stance::CROUCH);
		//HandleAction(action2);
		//Action* action3 = new Movement(GameObject("Male Toilet 2", new AbsoluteLocation()), Speed::RUN, Stance::STAND);
		//HandleAction(action3);
		//Action* action3 = new Movement(GameObject("Door", (RelativeLocation*) new ObjectRelativeLocation(Direction::LEFT, 2)), Speed::WALK);
		//HandleAction(action3);
		//Action* action4 = new OpenDoor(GameObject("Door", (RelativeLocation*) new ObjectRelativeLocation()));
		//HandleAction(action4);
		//Action* action5 = new Movement(GameObject((RelativeLocation*) new CharacterRelativeLocation(Direction::LEFT)), Speed::WALK, Stance::STAND);
		//HandleAction(action5);
		//Action* action7 = new Movement(GameObject((RelativeLocation*) new CharacterRelativeLocation(Direction::RIGHT)), Speed::WALK, Stance::STAND);
		//HandleAction(action7);
	/*	Action* action8 = new Pickup(GameObject("Rock", (RelativeLocation*) new ObjectRelativeLocation()));
		HandleAction(action8);
		Action* action2 = new Movement(GameObject("Room X", new AbsoluteLocation()), Speed::RUN, Stance::STAND);
		HandleAction(action2);
		Action* action9 = new Throw(GameObject((RelativeLocation*) new CharacterRelativeLocation(Direction::INFRONT, 10)));
		HandleAction(action9);*/
		//Action* action1 = new StairsMovement(GameObject("Basement", new AbsoluteLocation()), Speed::RUN);
		//HandleAction(action1);
		//Action* action2 = new StairsMovement(GameObject("Stairs", new StairsRelativeLocation(StairsRelativeLocation::StairsDirection::UP)), Speed::WALK);
		//HandleAction(action2);
		//Action* action1 = new ChangeStance(Stance::CROUCH);
		//HandleAction(action1);
		/*Action* action2 = new ChangeStance(Stance::CROUCH);
		HandleAction(action2);*/


		requestMove = false;
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "WebHandlerStructs/GameModeStructs.hpp"
#include "SpyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SPY_SPEAK_API USpyGameInstance : public UGameInstance
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable)
	void writeToLeaderboardData(int time_in_seconds, int gaurdsAliveNum, FString teamName);
	
	void my_to_json(json& j, const spy_game_mode::leaderboardDataEntry& x) {
		j = json{
			{ "team_name", x.teamName },
			{ "time", x.time }
		};
	}

	//void my_from_json(const json& j, spy_game_mode::leaderboardDataEntry& x) {
	//	x.teamName = j.at("team_name").get<std::string>();
	//	x.time = j.at("time").get<float>();
	//}

};

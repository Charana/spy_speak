// Fill out your copyright notice in the Description page of Project Settings.

#include "SpyGameModeBase.h"
#include "Cameras/Camera.h"




void ASpyGameModeBase::switchCamera(int feed_num, int camera_object_id)
{
	for (AActor* camera : cameras) {
		int32 uniqueID = camera->GetUniqueID();
		if (camera_object_id == uniqueID) {
			if (PlayerControllers.IsValidIndex(feed_num)) {
				APlayerController* cameraToChange = PlayerControllers[feed_num];
				
				ACamera* newCamera = Cast<ACamera>(camera);
				if (newCamera->GetController() == nullptr || 
					newCamera->GetController() != nullptr && !newCamera->GetController()->ActorHasTag("CameraController")) {
					APawn* oldPossessed = cameraToChange->GetPawn();
					if (oldPossessed != nullptr) {
						ACamera* oldCamera = Cast<ACamera>(oldPossessed);
						oldCamera->feed_num = -1;
						oldCamera->Tags.Remove("CurrentCamera");
					}
					
					newCamera->feed_num = feed_num;
					newCamera->Tags.Add("CurrentCamera");
					cameraToChange->Possess(newCamera);
				}
			}
		}
	}
}

#include "TimerManager.h"
float ASpyGameModeBase::getElapsedTime() {
	float timeElapsed = GetWorldTimerManager().GetTimerElapsed(countdownHandler);
	return timeElapsed;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API TransformData : public Data
{
public:
	FVector* location;
	FRotator* rotation;
	TransformData(FVector* location, FRotator* rotation) 
		: Data(Data::DataType::TRANSFORM_DATA), location(location), rotation(rotation) {}
};

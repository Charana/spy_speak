// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Stairs.h"
#include "Data.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API StairsData : public Data
{
public:
	AStairs* stairs;
	StairsData(AStairs* stairs) : Data(Data::DataType::STAIRS_DATA), stairs(stairs) {}
};

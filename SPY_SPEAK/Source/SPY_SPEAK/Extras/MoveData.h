// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CommandBNF/Actions/Speed.h"
#include "CoreMinimal.h"

class MoveData : public Data {
public:
	FVector dest;
	Speed speed;
	FVector faceDirection;
	MoveData(FVector dest, Speed speed) : MoveData(dest, speed, FVector::ZeroVector) {}
	MoveData(FVector dest, Speed speed, FVector faceDirection) : Data(DataType::MOVE_DATA), dest(dest), speed(speed), faceDirection(faceDirection) {}
};
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
 
#include "Actors/Door.h"
#include "Data.h"
#include "CoreMinimal.h"

class AActor;

class OpenData : public Data {
public:
	enum class OpenDirection { INWARD, OUTWARD };
	ADoor* door;
	OpenDirection openDirection;
	OpenData(ADoor* door, OpenDirection openDirection) : Data(DataType::OPEN_DATA), door(door), openDirection(openDirection) {}
};

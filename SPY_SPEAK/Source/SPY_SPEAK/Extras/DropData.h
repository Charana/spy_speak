// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API DropData : public Data
{
public:
	DropData() : Data(Data::DataType::DROP_DATA) {};
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "Character/Enemy.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API PickpocketData : public Data
{
public:
	AEnemy* enemyGaurd;
	PickpocketData(AEnemy* enemyGaurd) : enemyGaurd(enemyGaurd), Data(Data::DataType::PICKPOCKET_DATA) {};
};

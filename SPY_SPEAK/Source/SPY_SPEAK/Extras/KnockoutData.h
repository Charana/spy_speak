// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "Character/Enemy.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API KnockoutData : public Data
{
public:
	AEnemy* enemyGaurd;
	KnockoutData(AEnemy* enemyGaurd) : enemyGaurd(enemyGaurd), Data(Data::DataType::KNOCKOUT_DATA) {};
};

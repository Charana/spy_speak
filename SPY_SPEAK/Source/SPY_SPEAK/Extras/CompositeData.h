// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include <vector>
#include "CoreMinimal.h"

class CompositeData {
public:
	std::vector<Data*> actionDataList;
	CompositeData(std::vector<Data*> actionDataList) : actionDataList(actionDataList) {}
	void add(Data* data) { actionDataList.push_back(data); }
	void addAll(CompositeData* other) { this->actionDataList.insert(this->actionDataList.end(), other->actionDataList.begin(), other->actionDataList.end()); }
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class Data {
public:
	enum class DataType { OPEN_DATA, PICKUP_DATA, THROW_DATA, MOVE_DATA, STOP_DATA, STAIRS_DATA, CHANGE_STANCE_DATA, 
		HIDE_DATA, HACK_CAMERA_DATA, HACK_TERMINAL_DATA, TRANSFORM_DATA, RELATIVE_TRANSFORM_DATA, PICKPOCKET_DATA,
		DROP_DATA, STRANGLE_DATA, TURN_DATA, SNAP_ROTATION, STOP_GUARD_MOVEMENT_DATA };
	DataType dataType;
protected:
	Data(DataType dataType) : dataType(dataType) {};
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API StopData : public Data
{
public:
	StopData() : Data(DataType::STOP_DATA) {};
};

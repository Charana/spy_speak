// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "Character/Enemy.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API StrangleData : public Data
{
public:
	AEnemy* enemyGaurd;
	FVector forwardDisplacement;
	StrangleData(AEnemy* enemyGaurd, FVector forwardDisplacement)
		: enemyGaurd(enemyGaurd), Data(Data::DataType::STRANGLE_DATA), forwardDisplacement(forwardDisplacement) {};
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CommandBNF/Actions/Turn.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API TurnData : public Data
{
public:
	float yaw;
	Turn::Direction direction;
	TurnData(float yaw, Turn::Direction direction) 
		: Data(Data::DataType::TURN_DATA), yaw(yaw), direction(direction) {};
};

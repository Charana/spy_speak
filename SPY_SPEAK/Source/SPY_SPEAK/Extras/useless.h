// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "useless.generated.h"

UCLASS()
class SPY_SPEAK_API Auseless : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Auseless();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};

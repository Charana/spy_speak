// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "Character/Enemy.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API StopGuardMovementData : public Data
{
public:
	AEnemy* enemyGaurd;
	StopGuardMovementData(AEnemy* enemyGaurd)
		: enemyGaurd(enemyGaurd), Data(Data::DataType::STOP_GUARD_MOVEMENT_DATA) {};
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Extras/Data.h"
#include "CommandBNF/Actions/Stance.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API ChangeStanceData : public Data
{
public:
	Stance stance;
	ChangeStanceData(Stance stance) : Data(Data::DataType::CHANGE_STANCE_DATA), stance(stance) {};
};

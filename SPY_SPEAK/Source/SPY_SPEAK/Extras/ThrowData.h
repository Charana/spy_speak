// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "Actors/Pickupable.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API ThrowData : public Data
{
public:
	FVector throwLocation;
	float throwElevation;
	ThrowData(FVector throwLocation, float throwElevation) 
		: Data(DataType::THROW_DATA), throwLocation(throwLocation), throwElevation(throwElevation) {}
};

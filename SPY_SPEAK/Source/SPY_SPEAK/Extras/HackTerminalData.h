// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Terminal.h"
#include "Data.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API HackTerminalData : public Data
{
public:
	ATerminal* terminal;
	HackTerminalData(ATerminal* terminal) : Data(Data::DataType::HACK_TERMINAL_DATA), terminal(terminal) {};
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API RelativeTransformData : public Data
{
public:
	FVector location;
	FRotator rotation;
	RelativeTransformData(FVector location, FRotator rotation)
		: Data(Data::DataType::RELATIVE_TRANSFORM_DATA), location(location), rotation(rotation) {}
};

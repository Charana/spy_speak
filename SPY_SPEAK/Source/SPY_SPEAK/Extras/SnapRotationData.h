// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API SnapRotationData : public Data
{
public:
	FRotator rotation;
	SnapRotationData(FRotator rotation) : Data(Data::DataType::SNAP_ROTATION), rotation(rotation) {}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/Pickupable.h"
#include "Data.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API PickupData : public Data
{
public:
	enum class PickupLevel { NONE, FLOOR, WAIST };
	APickupable* actor;
	PickupLevel level;
	PickupData(APickupable* actor, PickupLevel level) : Data(DataType::PICKUP_DATA), actor(actor), level(level) {}
};

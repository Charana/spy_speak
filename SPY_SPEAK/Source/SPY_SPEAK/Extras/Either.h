// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

template <typename T>
class SPY_SPEAK_API Left
{
public:
	T value;
	Left(T value) : value(value) {};
};

template <typename U>
class SPY_SPEAK_API Right
{
public:
	U value;
	Right(U value) : value(value) {};
};

enum class Type { LEFT, RIGHT };

template <typename T, typename U>
class SPY_SPEAK_API Either
{
public:
	Either(Left<T> l) : l(new Left<T>(l.value)), type(Type::LEFT) {};
	Either(Right<U> r) : r(new Right<U>(r.value)), type(Type::RIGHT) {};
	
	Type getType() { return type;  }
	T getLeft() { return l->value;  }
	U getRight() { return r->value; }

protected:
	Either() {};
	Left<T>* l = nullptr;
	Right<U>* r = nullptr;
	Type type = -1;
};



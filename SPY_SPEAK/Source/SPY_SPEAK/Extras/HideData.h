// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Data.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API HideData : public Data
{
public:
	enum class FaceDirection { FACE_LEFT, FACE_RIGHT };
	FaceDirection faceDirection;
	HideData(FaceDirection faceDirection) : Data(DataType::HIDE_DATA), faceDirection(faceDirection) {}
};

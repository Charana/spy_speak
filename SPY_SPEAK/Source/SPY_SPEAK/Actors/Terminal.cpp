// Fill out your copyright notice in the Description page of Project Settings.

#include "Terminal.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"



// Sets default values
ATerminal::ATerminal() : Actionable(Actionable::ActionType::TERMINAL)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	RootComponent = Root;

	TableMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Table Mesh"));
	TableMesh->SetCollisionProfileName(FName("OverlapAll"));
	TableMesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	ChairMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Chair Mesh"));
	ChairMesh->SetCollisionProfileName(FName("OverlapAll"));
	ChairMesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	arrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow Component"));
	arrowComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	Tags.Add("terminal");
}

// Called when the game starts or when spawned
void ATerminal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATerminal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


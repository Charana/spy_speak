// Fill out your copyright notice in the Description page of Project Settings.

#include "Rotatable.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ARotatable::ARotatable() : AOpenable()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("ROOT"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESH"));
	Mesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	//MeshActor->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	AnimTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ANIMATION TRIGGER"));
	AnimTrigger->bGenerateOverlapEvents = true;
	AnimTrigger->OnComponentEndOverlap.AddDynamic(this, &ARotatable::startClosing);
	AnimTrigger->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ARotatable::BeginPlay()
{
	Super::BeginPlay();
	InitialRotation = TargetRotation = GetActorRotation();
	if (RotationAxis == "Yaw") TargetRotation.Yaw -= MaxRotation;
	else if (RotationAxis == "Pitch") TargetRotation.Pitch -= MaxRotation;
	else if (RotationAxis == "Roll") TargetRotation.Roll -= MaxRotation;
}

// Called every frame
void ARotatable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FRotator currentRotation = this->GetActorRotation();
	if (isOpening) {
		if (currentRotation.Yaw >= TargetRotation.Yaw) SetActorRotation(currentRotation + FRotator(0, -rate, 0));
		else isOpening = false;
	}
	if (isClosing) {
		if (currentRotation.Yaw <= InitialRotation.Yaw) SetActorRotation(currentRotation + FRotator(0, rate, 0));
		else isClosing = false;
	}
}

void ARotatable::open()
{
	UE_LOG(LogTemp, Warning, TEXT("startOpening"));
	Mesh->SetCanEverAffectNavigation(false);
	isClosing = false;
	isOpening = true;
}

void ARotatable::startClosing(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("startClosing"));
	isOpening = false;
	isClosing = true;
}



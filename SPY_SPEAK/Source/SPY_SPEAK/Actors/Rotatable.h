// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Openable.h"
#include "Curves/CurveFloat.h"
#include "Components/TimelineComponent.h"
#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Rotatable.generated.h"

UCLASS()
class SPY_SPEAK_API ARotatable : public AOpenable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARotatable();
	virtual void open() override;
	UPROPERTY(EditAnywhere)
	USceneComponent* Start;
	UPROPERTY(EditAnywhere)
	USceneComponent* End;

public:
	UFUNCTION()
	void startClosing(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

protected:
	bool isOpening = false;
	bool isClosing = false;
	FRotator InitialRotation;
	FRotator TargetRotation;

	UPROPERTY(EditAnywhere)
	USceneComponent* Root;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
	UBoxComponent* AnimTrigger;
	UPROPERTY(EditAnywhere, Category = "Animation")
	FString RotationAxis = "Yaw";
	UPROPERTY(EditAnywhere, Category = "Animation")
	float MaxRotation = 90;
	UPROPERTY(EditAnywhere, Category = "Animation")
	int rate = 5;

};

// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"


// Sets default values
ADoor::ADoor() : Actionable(Actionable::ActionType::DOOR)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("ROOT"));
	Root->SetRelativeScale3D(FVector(15.0f, 15.0f, 15.0f));

	inArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("IN ARROW"));
	inArrow->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	inArrow->SetRelativeLocation(FVector(50.0f, 30.0f, 0.0f));
	inArrow->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	outArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("OUT ARROW"));
	outArrow->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	outArrow->SetRelativeLocation(FVector(50.0f, -30.0f, 0.0f));
	outArrow->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


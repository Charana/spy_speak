// Fill out your copyright notice in the Description page of Project Settings.

#include "Stairs.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AStairs::AStairs() : Actionable(Actionable::ActionType::STAIRS)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	StairMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StairMesh"));
	StairMesh->SetCollisionProfileName(FName("BlockAll"));
	StairMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	Tags.Add(FName("stairs"));
}

ASpy* AStairs::getSpyCharacter()
{
	TArray<AActor*> outActors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("SpyCharacter"), outActors);
	if (outActors.IsValidIndex(0)) {
		ASpy* spy = Cast<ASpy>(outActors[0]);
		return spy;
	}
	return nullptr;
}



// Called when the game starts or when spawned
void AStairs::BeginPlay()
{
	Tags.Add(FName("Stairs"));
	Super::BeginPlay();
}

// Called every frame
void AStairs::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


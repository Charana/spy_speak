// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API Actionable
{
public:
	enum class ActionType { OPEN, PICKUP, STAIRS, TERMINAL, DOOR };
	ActionType actionType;
protected:
	Actionable() {}
	Actionable(ActionType actionType) : actionType(actionType) {};

};

// Fill out your copyright notice in the Description page of Project Settings.

#include "Openable.h"


// Sets default values
AOpenable::AOpenable() : Actionable(Actionable::ActionType::OPEN)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AOpenable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOpenable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.

#include "Helicopter.h"


// Sets default values
AHelicopter::AHelicopter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	EndGameSequenceBox = CreateDefaultSubobject<UBoxComponent>(TEXT("EndGameSequenceBox"));
	EndGameSequenceBox->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	Tags.Add(FName("Helicopter"));
}

// Called when the game starts or when spawned
void AHelicopter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHelicopter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actionable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Openable.generated.h"

UCLASS()
class SPY_SPEAK_API AOpenable : public AActor, public Actionable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOpenable();
	virtual void open() {};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
};

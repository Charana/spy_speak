// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actionable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Character/Spy.h"
#include "Stairs.generated.h"

UCLASS()
class SPY_SPEAK_API AStairs : public AActor, public Actionable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStairs();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* Root;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* StairMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FString, AActor*> directionToActor;

	UFUNCTION(BlueprintCallable)
	ASpy* getSpyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite)
	FString up = FString("up");
	UPROPERTY(BlueprintReadWrite)
	FString down = FString("down");
	UPROPERTY(BlueprintReadWrite)
	FString roof = FString("roof");
	UPROPERTY(BlueprintReadWrite)
	FString ground_floor = FString("ground_floor");
	UPROPERTY(BlueprintReadWrite)
	FString basement = FString("basement");
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actionable.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Terminal.generated.h"

UCLASS()
class SPY_SPEAK_API ATerminal : public AActor, public Actionable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATerminal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* Root;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* TableMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* ChairMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UArrowComponent* arrowComponent;

	UFUNCTION(BlueprintImplementableEvent)
	void startAudioAndVideo();
	UFUNCTION(BlueprintImplementableEvent)
	void stopAudioAndVideo();
};

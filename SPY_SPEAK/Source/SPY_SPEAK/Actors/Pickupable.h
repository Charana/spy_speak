// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actionable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/AudioComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/BoxComponent.h"
#include "Pickupable.generated.h"

UENUM(BlueprintType)
namespace PickupType {
	enum Type {
		NONE		UMETA(DisplayName = "NONE"),
		ROCK		UMETA(DisplayName = "ROCK"),
		MUG			UMETA(DisplayName = "MUG"),
		BEAKER		UMETA(DisplayName = "BEAKER"),
		COFFEE_CUP	UMETA(DisplayName = "COFFEE_CUP"),
		BOTTLE		UMETA(DisplayName = "BOTTLE"),
		CAN			UMETA(DisplayName = "CAN"),
		HANDSAW		UMETA(DisplayName = "HANDSAW")
	};
}

UCLASS()
class SPY_SPEAK_API APickupable : public AActor, public Actionable
{
	GENERATED_BODY()
	
public:

	// Sets default values for this actor's properties
	APickupable();
	virtual void pickup();

	virtual void setProjectileMovementParameters(FVector direction, float speed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<PickupType::Type> type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPrimitiveComponent* Root;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UProjectileMovementComponent* ProjectileMovComp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAudioComponent* AudioComp;

	virtual void NotifyHit(
		class UPrimitiveComponent * MyComp,
		AActor * Other,
		class UPrimitiveComponent * OtherComp,
		bool bSelfMoved,
		FVector HitLocation,
		FVector HitNormal,
		FVector NormalImpulse,
		const FHitResult & Hit) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	float ShinyAmount = 1.0f;;
	bool Shiny = false;
	UPROPERTY(EditAnywhere)
	UTexture* DefaultTexture;
	UMaterialInstanceDynamic* MaterialInstance;
	
};


// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickupable.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Hearing.h"

// Sets default values
APickupable::APickupable() : Actionable(Actionable::ActionType::PICKUP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	Root->SetRelativeScale3D(FVector(9.0f, 9.0f, 9.0f));
	Root->SetCollisionProfileName(FName("BlockAll"));
	Root->bGenerateOverlapEvents = false;
	Root->bShouldUpdatePhysicsVolume = false;
	RootComponent = Root;

	ProjectileMovComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovComp->UpdatedComponent = Root;
	ProjectileMovComp->InitialSpeed = 0.0f;
	ProjectileMovComp->MaxSpeed = 0.0f;
	//ProjectileMovComp->SetVelocityInLocalSpace(FVector::ZeroVector);
	ProjectileMovComp->Velocity = FVector::ZeroVector;
	ProjectileMovComp->bRotationFollowsVelocity = true;
	ProjectileMovComp->bShouldBounce = true;
	ProjectileMovComp->Bounciness = 0.2;
	ProjectileMovComp->Friction = 0.3;
	ProjectileMovComp->BounceVelocityStopSimulatingThreshold = 500.0f;

	AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
	AudioComp->SetAutoActivate(false);

	Tags.Add("pickupable");
}

void APickupable::pickup()
{
	this->Destroy();
}

void APickupable::setProjectileMovementParameters(FVector direction, float speed)
{
	//ProjectileMovComp->InitialSpeed = speed;
	//ProjectileMovComp->MaxSpeed = speed;
	Root->bShouldUpdatePhysicsVolume = false;
	ProjectileMovComp->Velocity = direction * speed;
	UE_LOG(LogTemp, Warning, TEXT("ProjectileVelocity[%s]"), *ProjectileMovComp->Velocity.ToString());
}

// Called when the game starts or when spawned
void APickupable::BeginPlay()
{
	Super::BeginPlay();
	ProjectileMovComp->Velocity = FVector(0, 0, -1);
	//MaterialInstance = Mesh->CreateDynamicMaterialInstance(0);
	//if (MaterialInstance != nullptr) {
	//	MaterialInstance->SetTextureParameterValue(TEXT("Default Texture"), DefaultTexture);
	//}
}

// Called every frame
void APickupable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//if (Shiny) { //Whether ShinyAmount == 1
	//	ShinyAmount = FMath::Clamp(ShinyAmount - DeltaTime, 0.0f, 1.0f);
	//	if (ShinyAmount == 0.0f) Shiny = false;
	//}
	//else { 
	//	ShinyAmount = FMath::Clamp(ShinyAmount + DeltaTime, 0.0f, 1.0f);
	//	if (ShinyAmount == 1.0f) Shiny = true;
	//}
	//
	//if (MaterialInstance != nullptr) {
	//	//UE_LOG(LogTemp, Warning, TEXT("Shiny Amount %f"), ShinyAmount);
	//	MaterialInstance->SetScalarParameterValue(TEXT("Shiny Amount"), ShinyAmount);;
	//}
}

#include "Character/Enemy.h"
#include "AIController.h"
#include "BrainComponent.h"

void APickupable::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved,
	FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit) {

	UE_LOG(LogTemp, Warning, TEXT("Projectile[%s]"), *MyComp->GetName());
	UE_LOG(LogTemp, Warning, TEXT("HitActor[%s]"), *OtherComp->GetName());
	//float impulseSize = NormalImpulse.Size();
	float impulseSize = ProjectileMovComp->Velocity.Size(); //Fake physics
	UE_LOG(LogTemp, Warning, TEXT("HitImpulse[%f]"), impulseSize);
	if (impulseSize > 3000.0f) {
		impulseSize = impulseSize / 1000;
		//Play Sound (not at location)
		AudioComp->SetVolumeMultiplier(impulseSize);
		AudioComp->Play();

		if (IsValid(Other)) {
			if (Other->ActorHasTag("enemy")) {
				AEnemy* guard = Cast<AEnemy>(Other);
				if (guard == nullptr) {
					UE_LOG(LogTemp, Warning, TEXT("NULLPTR GUARD"));
				}
				guard->setIsDead();
				guard->knockedOut = true;
			}
		}
		else {
			//Report Noise Event (AI Perception)
			UAISense_Hearing::ReportNoiseEvent(GetWorld(), this->GetActorLocation(), 1.0f, this);
		}
	}
}


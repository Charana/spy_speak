// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actionable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Door.generated.h"

UCLASS()
class SPY_SPEAK_API ADoor : public AActor, public Actionable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* Root;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UArrowComponent* inArrow;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UArrowComponent* outArrow;

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation Montages")
	void startDoorOpenInwardMontage();
	UFUNCTION(BlueprintImplementableEvent, Category = "Animation Montages")
	void startDoorOpenOutwardMontage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.

#include "SpyGameInstance.h"


void USpyGameInstance::writeToLeaderboardData(int time_in_seconds, int gaurdsAliveNum, FString teamName)
{
	std::string finalpath = "D:/Documents/Unreal Projects/SpySpeak/SPY_SPEAK/Public/leaderboard.txt";
	std::ifstream file(finalpath);
	UE_LOG(LogTemp, Warning, TEXT("FINAL-PATH: %s"), *FString(finalpath.c_str()))
		if (file.is_open()) {
			//Read leaderboard file
			std::string fileline, filelines;
			while (std::getline(file, fileline)) filelines += fileline + "\n";
			file.close();
			//Serialize content to c++ object
			json leaderboardDataJSON = filelines;
			std::vector<json> leaderboardData = leaderboardDataJSON.get<std::vector<json>>();
			//Modify leaderboard data
			int points = 100 * gaurdsAliveNum + time_in_seconds;
			spy_game_mode::leaderboardDataEntry entry(TCHAR_TO_UTF8(*teamName), points);
			json entryJSON;
			my_to_json(entryJSON, entry);
			//Deserialize to JSON
			leaderboardData.push_back(entryJSON);
			json newLeaderboardDATAJSON = leaderboardData;

			//Write back
			std::ofstream file;
			file.open(finalpath);
			if (file.is_open()) {
				std::string deserializedString = newLeaderboardDATAJSON.dump();
				file.write(deserializedString.c_str(), deserializedString.length());
				file.close();
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("Failed to WRITE leaderboard file"));
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Failed to READ leaderboard file"));
		}
}

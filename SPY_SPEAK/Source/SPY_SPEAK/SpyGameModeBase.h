// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Terminal.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SpyGameModeBase.generated.h"


UCLASS()
class SPY_SPEAK_API ASpyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	FString team_name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* currentObjective;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int terminalsHacked = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTimerHandle countdownHandler;

	float getElapsedTime();

	//UFUNCTION(BlueprintImplementableEvent, Category="Travel")
	//void Basement();

	//UFUNCTION(BlueprintImplementableEvent, Category="Travel")
	//void FirstFloor();

	UFUNCTION(BlueprintImplementableEvent)
	void setNextTerminal();

	UFUNCTION(BlueprintImplementableEvent)
	void guardKilled();

	UFUNCTION(BlueprintImplementableEvent)
	TArray<FVector> getGaurdPositions();
	
	UFUNCTION(BlueprintImplementableEvent)
	void addHackedCameras();

	UFUNCTION(BlueprintImplementableEvent)
	void startGame();

	UPROPERTY(BlueprintReadWrite)
	TArray<APlayerController*> PlayerControllers;

	UPROPERTY(BlueprintReadWrite)
	TArray<AActor*> cameras;

	UFUNCTION(BlueprintCallable)
	void switchCamera(int feed_num, int camera_object_id);
};

//
//  object_parser.hpp
//  EngineCommandSet
//
//  Created by Albie Baker-Smith on 17/11/2017.
//  Copyright © 2017 Albie Baker-Smith. All rights reserved.
//

#ifndef GAME_MODE_STRUCTS_HPP
#define GAME_MODE_STRUCTS_HPP

#include "CommandBNF/Parsing/json_library.hpp"

using nlohmann::json;

namespace spy_game_mode {
	class leaderboardDataEntry {
	public:
		std::string teamName;
		int time;

		leaderboardDataEntry() {};
		leaderboardDataEntry(std::string teamName, int time)
			: teamName(teamName), time(time) {};
	};
}

#endif

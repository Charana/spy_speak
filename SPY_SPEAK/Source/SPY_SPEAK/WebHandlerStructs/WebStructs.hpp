//
//  object_parser.hpp
//  EngineCommandSet
//
//  Created by Albie Baker-Smith on 17/11/2017.
//  Copyright © 2017 Albie Baker-Smith. All rights reserved.
//

#ifndef WEB_STRUCTS_HPP
#define WEB_STRUCTS_HPP

#include "CommandBNF/Parsing/json_library.hpp"

using nlohmann::json;

namespace webstructs {
    

	class Vector2D {
	public:
		float X;
		float Y;
		Vector2D(float X, float Y) : X(X), Y(Y) {};
	};

	void to_json(json& j, const Vector2D& x) {
		j = json{ { "x", x.X },{ "y", x.Y } };
	}

	void from_json(const json& j, Vector2D& x) {
		x.X = j.at("X").get<float>();
		x.Y = j.at("Y").get<float>();
	}

	class levelEdges {
	public:
		float minX;
		float minY;
		float maxX;
		float maxY;

		levelEdges(float minX, float minY, float maxX, float maxY) :
			minX(minX), minY(minY), maxX(maxX), maxY(maxY) {};
	};

	void to_json(json& j, const levelEdges& x) {
		j = json{ 
			{ "min_x", x.minX }, 
			{ "min_y", x.minY },
		    { "max_x", x.maxX },
		    { "max_y", x.maxY }
		};
	}

	class CameraData {
	public:
		Vector2D location;
		int feed_num;
		int radius;
		int camera_id;
		float dir_rad;
		CameraData(Vector2D location, int feed_num, int radius, int camera_id, float dir_rad)
			: location(location), feed_num(feed_num), radius(radius), camera_id(camera_id), dir_rad(dir_rad) {};
	};

	void to_json(json& j, const CameraData& x) {
		if (x.feed_num == -1) {
			j = json{
				{ "loc", x.location },
				{ "feed_index", nullptr },
				{ "max_visibility_dist", x.radius },
				{ "id", x.camera_id },
				{ "dir_rad", x.dir_rad }
			};
		}
		else {
			j = json{
				{ "loc", x.location },
				{ "feed_index", x.feed_num },
				{ "max_visibility_dist", x.radius },
				{ "id", x.camera_id },
				{ "dir_rad", x.dir_rad }
			};
		}
	}

	class GaurdData {
	public:
		std::string direction;
		float distance;
		GaurdData(std::string direction, float distance)
			: direction(direction), distance(distance) {};
	};

	void to_json(json& j, const GaurdData& x) {
		j = json{
			{ "direction", x.direction },
			{ "distance", x.distance }
		};
	}

	//class leaderboardDataEntry {
	//public:
	//	std::string teamName;
	//	int time;

	//	leaderboardDataEntry(std::string teamName, int time)
	//		: teamName(teamName), time(time) {};
	//};

	//void to_json(json& j, const leaderboardDataEntry& x) {
	//	j = json{
	//		{ "team_name", x.teamName },
	//		{ "time", x.time }
	//	};
	//}

	//void to_json(json& j, const std::vector<leaderboardDataEntry>& vecX) {
	//	std::vector<std::string> leaderboardDataEntryString;
	//	for (leaderboardDataEntry entry : vecX) {
	//		leaderboardDataEntryString.push_back(json(entry).dump());
	//	}
	//	j = json(leaderboardDataEntryString);
	//}

	//void from_json(const json& j, leaderboardDataEntry& x) {
	//	x.teamName = j.at("team_name").get<std::string>();
	//	x.time = j.at("time").get<float>();
	//}

}

#endif

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Connection.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <unordered_map>
#include "SpyGameModeBase.h"
#include "Character/Spy.h"
#include "WebHandlerActor.generated.h"

UCLASS()
class SPY_SPEAK_API AWebHandlerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AWebHandlerActor();
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	UResponse* readFile(FString filepath);

	UFUNCTION(BlueprintCallable)
	void setSpyCharacter(AActor* character);

	UFUNCTION(BlueprintCallable)
	UResponse* readFileBinary(FString relativepath);

	UFUNCTION(BlueprintCallable)
	UResponse* getEdges();
	
	//UFUNCTION(BlueprintCallable)
	//UResponse* getSpyPosition();

	//UFUNCTION(BlueprintCallable)
	//UResponse* getGaurdPositions();

	UFUNCTION(BlueprintCallable)
	UResponse* getTerminalsHacked();

	UFUNCTION(BlueprintCallable)
	UResponse* getshouldMinigameStart();

	UFUNCTION(BlueprintCallable)
	UResponse* get3DDistanceToNextTerminal();

	UFUNCTION(BlueprintCallable)
	UResponse* getPositions(UConnection* connection);

	UFUNCTION(BlueprintCallable)
	UResponse* hackingTerminalChallengeResult(UConnection* connection);

	UFUNCTION(BlueprintCallable)
	UResponse* setTeamName(UConnection* connection);

	UFUNCTION(BlueprintCallable)
	UResponse* setCamera(UConnection* connection);

	UFUNCTION(BlueprintCallable)
	UResponse* getMaxObjectiveDist();

	UFUNCTION(BlueprintCallable)
	UResponse* answerQuestion(UConnection* connection);

	UFUNCTION(BlueprintCallable)
	UResponse* getTerminalLocation();

public:
	bool shouldMinigameStart;
	int terminalsHacked;
	ASpyGameModeBase* spyGameMode;
	std::string currentItem;

protected:
	std::unordered_map<int, TArray<AActor*>> floorToCameraList;
	int OK = 200;
	float cameraSightRadius = 10000.f;
	float spySightRadius = 40000.f;

	ASpy* spyCharacter;
	ASpy* getSpyCharacter();
};

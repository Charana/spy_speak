// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Extras/Data.h"
#include <queue>
#include <Containers/Queue.h>
#include <deque>
#include <string>
#include "Connection.h"
#include "CommandBNF/Actions/Action.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActionHandler.generated.h"

class ASpyController;

UCLASS()
class SPY_SPEAK_API AActionHandler : public AActor
{
	GENERATED_BODY()
public:
	AActionHandler();
	//Will call the action visitor implementation on the Action to return a location that will be sent to SpyController 
	void EvaluateAction(Action* action);
	void HandleEvaluatedAction(Action* action, Either<CompositeData*, std::string> result);
	void queueAction(Action* action);
	void nextAction();
	void InitController(ASpyController* spyController);

	UFUNCTION(BlueprintCallable)
	void ExecuteJSONAsAction(UConnection* connection);

	//UPROPERTY(BlueprintReadWrite)
	//std::queue<Action*> delayedActions;

	//UPROPERTY(BlueprintReadWrite)
	TQueue<Action*> TQueueDelayedActions;

	UFUNCTION(BlueprintCallable)
	void QueueStop();

	UFUNCTION(BlueprintCallable)
	void QueueConditionalAttack();

protected:
	bool requestMove = true;
	ASpyController* spyController;

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

	bool is_expecting_response = false;
	UConnection* current_connection = nullptr;

	Action* currentAction;
};

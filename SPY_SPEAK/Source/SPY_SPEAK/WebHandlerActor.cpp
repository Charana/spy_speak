// Fill out your copyright notice in the Description page of Project Settings.

#include "WebHandlerActor.h"
#include "IHttpResponse.h"
#include <iostream>
#include <string>
#include <fstream>
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "WebHandlerStructs/WebStructs.hpp"
#include "GameFramework/GameModeBase.h"
#include <vector>
#include "Cameras/Camera.h"
#include "Engine.h"

// Sets default values
AWebHandlerActor::AWebHandlerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

}

void AWebHandlerActor::BeginPlay()
{
	//Super::BeginPlay();
	//TArray<AActor*> outActors1;
	//UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("SpyCharacter"), outActors1);
	//if (!outActors1.IsValidIndex(0)) { UE_LOG(LogTemp, Warning, TEXT("No Nav Mesh Actor")); }
	//else spyCharacter = outActors1[0];

	TArray<AActor*> cameraActors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("Camera"), cameraActors);
	for (AActor* cameraActor : cameraActors) {
		int floorNum = -1;
		float depth = cameraActor->GetActorLocation().Z;
		if (depth > 12710.f && depth < 18370.f) floorNum = 0;		//Basement - 13710.0 to 18370.0
		else if (depth > 19700.f && depth < 25800.f) floorNum = 1;	//First Floor - 19700.0 to 25800.0
		else if (depth > 25800.f) floorNum = 2; 					//Roof - 25800.0 upwards

		if (floorNum == -1) {
			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Camera floor could not be found"));
			continue;
		}
		floorToCameraList[floorNum].Add(cameraActor);
	}

	spyGameMode = (ASpyGameModeBase*) GetWorld()->GetAuthGameMode();
	if (spyGameMode == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Game Mode cannot be found"));
	}
}


#include <stdio.h>  /* defines FILENAME_MAX */
#include <direct.h>
#define GetCurrentDir _getcwd
std::string getCurrentDirectory()
{
	char cCurrentPath[FILENAME_MAX];
	if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath))){
		return std::string();
	}
	else {
		cCurrentPath[sizeof(cCurrentPath) - 1] = '\0'; /* not really required */
		return std::string(cCurrentPath);
	}
}

const wchar_t* GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	std::wstring wc_str(cSize, L'#');
	mbstowcs(&wc_str[0], c, cSize);
	const wchar_t* wc = wc_str.c_str();
	return wc;
}

#include <windows.h>
std::string myGetFullPathName()
{
	TCHAR filePath[MAX_PATH];
	const wchar_t* fileName = GetWC("D:minimap.html");
	GetFullPathName(fileName, MAX_PATH, filePath, 0);
	std::string filePathStr(TCHAR_TO_UTF8(filePath));
	return filePathStr;
}

std::wstring getExecutablePath()
{
	WCHAR result[MAX_PATH];
	return std::wstring(result, GetModuleFileName(NULL, result, MAX_PATH));
}

FString relative_to_abs(FString relpath) {
	FString rel = FPaths::GameContentDir();
	FString full = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*rel);
	return full.Append("Public/").Append(relpath);
}

UResponse* AWebHandlerActor::readFile(FString relativepath)
{
	//std::string directoryPath = ExePath();
	//UE_LOG(LogTemp, Warning, TEXT("DIRECTORY-PATH: %s"), *FString(directoryPath.c_str()))

	FString fullPathFString = relative_to_abs(relativepath);
	std::string finalpath(TCHAR_TO_UTF8(*fullPathFString));
	UE_LOG(LogTemp, Warning, TEXT("CURRENT DIRECTORY[%s]"), *FString(finalpath.c_str()));
	std::ifstream file(finalpath);
	UE_LOG(LogTemp, Warning, TEXT("FINAL-PATH: %s"), *FString(finalpath.c_str()))
	if (!file.is_open()) {
		UE_LOG(LogTemp, Warning, TEXT("File could not be found: %s\n"), *FString(finalpath.c_str()));
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString("No File"));
		response->SetResponseContentType(EMediaType::TEXT_PLAIN);
		return response;
	}
	else {
		std::string fileline, filelines;
		while (std::getline(file, fileline)) filelines += fileline + "\n";
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(filelines.c_str()));
		return response;
	}
}


void AWebHandlerActor::setSpyCharacter(AActor* character){
	spyCharacter = Cast<ASpy>(character);
}

#include <vector>
#include "Misc/Base64.h"
UResponse* AWebHandlerActor::readFileBinary(FString relativepath)
{
	FString fullPathFString = relative_to_abs(relativepath);
	std::string finalpath(TCHAR_TO_UTF8(*fullPathFString));
	UE_LOG(LogTemp, Warning, TEXT("CURRENT DIRECTORY[%s]"), *FString(finalpath.c_str()));
	//std::ifstream file(finalpath, std::ios::in || std::ios::binary);
	//if (!file.is_open()) {
	//	UE_LOG(LogTemp, Warning, TEXT("Binary file could not be found: %s\n"), *FString(finalpath.c_str()));
	//	UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString("No Binary File"));
	//	response->SetResponseContentType(EMediaType::TEXT_PLAIN);
	//	return response;
	//}
	//else {
		UE_LOG(LogTemp, Warning, TEXT("Opened PNG"));
		//// stop eating new lines
		//file.unsetf(std::ios::skipws);
		//// get its size:
		//std::streampos fileSize;
		//file.seekg(0, std::ios::end);
		//fileSize = file.tellg();
		//file.seekg(0, std::ios::beg);
		//// reserve capacity
		//std::vector<unsigned char> vec;
		//vec.reserve(fileSize);
		//// read the data:
		//vec.insert(vec.begin(), std::istream_iterator<unsigned char>(file), std::istream_iterator<unsigned char>());

		//std::vector<char> buffer;
		//buffer.assign(vec.begin(), vec.end());
		//TArray<unsigned char> buffer = vec;
		//UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(vec->data()));s
		//response->SetResponseContentType(EMediaType::IMAGE_PNG);

		TArray<unsigned char> UpFileRawData;
		FFileHelper::LoadFileToArray(UpFileRawData, *FString(finalpath.c_str()));
		FString base64PNGFile = FBase64::Encode(UpFileRawData);

		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), base64PNGFile);
		response->SetResponseContentType(EMediaType::IMAGE_PNG);
		return response;
	//}

}

///////////////////////////////////////////////////////////////////////////////////////
//// TexturePath contains the local file full path

//// file name
//int32 LastSlashPos;
//TexturePath.FindLastChar('/', LastSlashPos);
//FString FileName = TexturePath.RightChop(LastSlashPos + 1);

//// get data
//TArray<uint8> UpFileRawData;
//FFileHelper::LoadFileToArray(UpFileRawData, *TexturePath);

//// prepare json data
//FString JsonString;
//TSharedRef<TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<TCHAR>::Create(&JsonString);

//JsonWriter->WriteObjectStart();
//JsonWriter->WriteValue("FileToUpload", FileName);
//JsonWriter->WriteValue("ImageData", FBase64::Encode(UpFileRawData));
//JsonWriter->WriteObjectEnd();
//JsonWriter->Close();

//// the json request
//TSharedRef<IHttpRequest> SendJsonRequest = (&FHttpModule::Get())->CreateRequest();
//SendJsonRequest->OnProcessRequestComplete().BindRaw(this, &SYagOpenFileWidget::HttpUploadYagFileComplete);
//SendJsonRequest->SetURL("http://myserver/MyJsonHandlingPhpScript.php");
//SendJsonRequest->SetVerb("POST");
//SendJsonRequest->SetContentAsString(JsonString);
//SendJsonRequest->ProcessRequest();
///////////////////////////////////////////////////////////////////////////////////////

UResponse* AWebHandlerActor::getEdges() {
	TArray<AActor*> outActors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("NavMesh"), outActors);
	if (!outActors.IsValidIndex(0)) {
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString("No Nav Mesh Actor"));
		response->SetResponseContentType(EMediaType::TEXT_PLAIN);
		return response;
	}
	else {
		FVector origin3D, boxExtent3D;
		outActors[0]->GetActorBounds(false, origin3D, boxExtent3D);
		FVector2D origin(origin3D);
		float halfWidth = boxExtent3D.X;
		float halfHeight = boxExtent3D.Y;
		webstructs::Vector2D TopLeft = webstructs::Vector2D(origin.X - halfWidth, origin.Y - halfHeight);
		webstructs::Vector2D BottomRight = webstructs::Vector2D(origin.X + halfWidth, origin.Y + halfHeight);
		webstructs::levelEdges LevelEdges = webstructs::levelEdges(TopLeft.X, TopLeft.Y, BottomRight.X, BottomRight.Y);
		json j = LevelEdges;
		FString json = FString(j.dump().c_str());
		UE_LOG(LogTemp, Warning, TEXT("JSON - %s\n"), *json);
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), json);
		response->SetResponseContentType(EMediaType::APPLICATION_JSON);
		return response;
	}
}


UResponse* AWebHandlerActor::getMaxObjectiveDist() {
	json j;
	j["max_distance"] = 25000;
	FString json = FString(j.dump().c_str());
	UResponse* response = UResponse::ConstructResponseExt(GetWorld(), json);
	response->SetResponseContentType(EMediaType::APPLICATION_JSON);
	return response;
}

#include "Kismet/KismetMathLibrary.h"
#include <math.h>
UResponse* AWebHandlerActor::getPositions(UConnection* connection)
{
	FString floorJSONFString = connection->GetData();
	FString contentLengthFString = connection->GetHeader(FString("Content-Length"));
	int contentLength = std::stoi(std::string(TCHAR_TO_UTF8(*contentLengthFString)));
	std::string floorJSONString = std::string(TCHAR_TO_UTF8(*floorJSONFString)).substr(0, contentLength);
	json floorJSON = json::parse(floorJSONString);
	int userRequestedFloorNum = floorJSON["floor_num"].get<int>();

	if (spyCharacter != nullptr) {

		//Get Spy floor num
		int floorNum = spyCharacter->getCurrentFloor();
		FVector spyPosVect = spyCharacter->GetActorLocation();

		//If floorNumber isn't specified by user, get the current spys floor data
		if (userRequestedFloorNum == -1) userRequestedFloorNum = floorNum;

		//Get Current Spy Forward Vector
		FVector forwardVector = spyCharacter->GetActorForwardVector();
		float dir_rad = atan2(forwardVector.Y, forwardVector.X);

		//Get Spy Data
		webstructs::Vector2D spyPosVec2D = webstructs::Vector2D(spyPosVect.X, spyPosVect.Y);
		
		//Get Gaurd Data (Contingent on floor number)
		//TArray<AActor*> currentCameras, currentCamerasOnRequestFloor;
		//UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("CurrentCamera"), currentCameras);
		//currentCamerasOnRequestFloor = currentCameras.FilterByPredicate([=](const AActor* currentCamera) {
		//	FVector cameraLocation = currentCamera->GetActorLocation();
		//	int cameraFloorNum = spyCharacter->getFloor(cameraLocation);
		//	return cameraFloorNum == userRequestedFloorNum;
		//});
		//std::vector<webstructs::Vector2D> guardPositions;
		//for (AActor* camera : currentCamerasOnRequestFloor) {
		//	TArray<FOverlapResult> overlappingResults;
		//	FVector cameraPosition = camera->GetActorLocation();
		//	
		//	TArray<AActor*> guards;
		//	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("enemy"), guards);
		//	for (AActor* guard : guards) {
		//		FVector guardLocation = guard->GetActorLocation();
		//		float distance2D = (guardLocation - cameraPosition).Size2D();
		//		if (distance2D > cameraSightRadius) guardPositions.push_back(webstructs::Vector2D(guardLocation.X, guardLocation.Y));
		//	}
		//}

		//Get Gaurd Data (Contingent on floor number)
		std::vector<webstructs::Vector2D> guardPositions;
		//Get Gaurd Data (Contingent on floor number)
		TArray<AActor*> guards;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("enemy"), guards);
		for (AActor* guard : guards) {
			FVector guardLocation = guard->GetActorLocation();
			int guardFloorNum = spyCharacter->getFloor(guardLocation);
			if (guardFloorNum == userRequestedFloorNum) guardPositions.push_back(webstructs::Vector2D(guardLocation.X, guardLocation.Y));
		}


		//Get Camera Data (Cameras on current floor)
		std::vector<webstructs::CameraData> cameraDatas;
		for (AActor* actor : floorToCameraList[userRequestedFloorNum]){
			ACamera* cameraActor = Cast<ACamera>(actor);
			if (cameraActor->isHacked) {
				FVector cameraLocation = cameraActor->GetActorLocation();
				webstructs::Vector2D cameraLocation2D = webstructs::Vector2D(cameraLocation.X, cameraLocation.Y);
				int feed_num = cameraActor->feed_num;
				int camera_id = cameraActor->GetUniqueID();
				FVector cameraForwardVector = cameraActor->GetActorForwardVector();
				float dir_rad = atan2(cameraForwardVector.Y, cameraForwardVector.X);

				webstructs::CameraData cameraData(cameraLocation2D, feed_num, cameraSightRadius, camera_id, dir_rad);
				cameraDatas.push_back(cameraData);
			}
		}

		FVector terminalLocation = spyGameMode->currentObjective->GetActorLocation();
		webstructs::Vector2D terminalLocation2D = webstructs::Vector2D(terminalLocation.X, terminalLocation.Y);
		float terminalZ = terminalLocation.Z;
		int terminalFloor = -1;
		if (terminalZ > 12710.f && terminalZ < 18370.f) terminalFloor = 0;		//Basement - 13710.0 to 18370.0
		else if (terminalZ > 19700.f && terminalZ < 25800.f) terminalFloor = 1;	//First Floor - 19700.0 to 25800.0
		else if (terminalZ > 25800.f) terminalFloor = 2; 							//Roof - 25800.0 upwards

		json j = json({
			{ "spy", {
				{ "loc", spyPosVec2D }, 
				{ "dir_rad", dir_rad },
				{ "floor_index", floorNum } 
			}},
			{ "guards_locs", json(guardPositions) },
			{ "cameras", json(cameraDatas) },
			{ "target", {
				{ "loc", terminalLocation2D },
				{"floor_index", terminalFloor }
			}}
		});
		
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
		response->SetResponseContentType(EMediaType::APPLICATION_JSON);
		return response;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("SpyCharacter is null"));
		UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString("SpyCharacter is null"));
		response->SetResponseContentType(EMediaType::TEXT_PLAIN);
		return response;
	}
}

//URI - "/terminal-hacked"
UResponse* AWebHandlerActor::getTerminalsHacked()
{
	json j;
	j["terminals_hacked"] = spyGameMode->terminalsHacked;;
	UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	response->SetResponseContentType(EMediaType::APPLICATION_JSON);
	return response;
}


UResponse* AWebHandlerActor::get3DDistanceToNextTerminal()
{
	FVector terminalLocation = spyGameMode->currentObjective->GetActorLocation();
	FVector spyLocation = spyCharacter->GetActorLocation();
	spyLocation.Z = 0; terminalLocation.Z = 0;
	float dist3D = FVector::Distance(terminalLocation, spyLocation);
	json j; j["distance"] = dist3D;
	UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	response->SetResponseContentType(EMediaType::APPLICATION_JSON);
	return response;
}

#include "Engine/World.h"
UResponse* AWebHandlerActor::setTeamName(UConnection* connection)
{
	FString teamNameJSONFString = connection->GetData();
	FString contentLengthFString = connection->GetHeader(FString("Content-Length"));
	int contentLength = std::stoi(std::string(TCHAR_TO_UTF8(*contentLengthFString)));
	std::string teamNameJSONString = std::string(TCHAR_TO_UTF8(*teamNameJSONFString)).substr(0, contentLength);
	json teamNameJSON = json::parse(teamNameJSONString);
	std::string team_name = teamNameJSON["team_name"].get<std::string>();

	spyGameMode->team_name = FString(team_name.c_str());
	spyGameMode->startGame();

	UResponse* response = UResponse::ConstructResponse(GetWorld());
	response->SetResponseStatusCode(EHttpStatusCode(OK));
	return response;
}

UResponse* AWebHandlerActor::setCamera(UConnection* connection)
{
	FString setCameraJSONFString = connection->GetData();
	FString contentLengthFString = connection->GetHeader(FString("Content-Length"));
	int contentLength = std::stoi(std::string(TCHAR_TO_UTF8(*contentLengthFString)));
	std::string setCameraJSONString = std::string(TCHAR_TO_UTF8(*setCameraJSONFString)).substr(0, contentLength);
	json setCameraJSON = json::parse(setCameraJSONString);
	int feed_num = setCameraJSON["replace_feed_index"].get<int>();
	int camera_num = setCameraJSON["new_camera_game_id"].get<int>();

	spyGameMode->switchCamera(feed_num, camera_num);
	
	UResponse* response = UResponse::ConstructResponse(GetWorld());
	response->SetResponseStatusCode(EHttpStatusCode(OK));
	return response;
}

std::string angleToDirection(float yaw) {
	if (yaw >= -90 && yaw <= 90) return "forward";
	else if (yaw >= -180 && yaw <= 0)  return "left";
	else if (yaw >= 90 && yaw <= -90) return "backward";
	else if(yaw >= 0 && yaw <= 180) return "right";
	return "none";
}

UResponse* AWebHandlerActor::answerQuestion(UConnection* connection)
{
	UE_LOG(LogTemp, Warning, TEXT("Inside Answer Question"));
	FString setCameraJSONFString = connection->GetData();
	FString contentLengthFString = connection->GetHeader(FString("Content-Length"));
	int contentLength = std::stoi(std::string(TCHAR_TO_UTF8(*contentLengthFString)));
	std::string questionJSONString = std::string(TCHAR_TO_UTF8(*setCameraJSONFString)).substr(0, contentLength);
	json questionJSON = json::parse(questionJSONString);
	std::string question_type = questionJSON["type"].get<std::string>();

	UResponse* response = nullptr;
	if (question_type == "inventory_question") {
		UE_LOG(LogTemp, Warning, TEXT("Inside Inventory Question"));
		json j;
		j["type"] = !currentItem.compare("none") ? "success" : "failure";
		j["inventory_item"] = currentItem;
		response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	}
	else if (question_type == "guards_question") {
		UE_LOG(LogTemp, Warning, TEXT("Inside Guards Question"));
		TArray<FOverlapResult> overlappingResults;
		FVector spyLocation = spyCharacter->GetActorLocation();
		GetWorld()->OverlapMultiByObjectType(overlappingResults, spyLocation, FQuat(), FCollisionObjectQueryParams(ECollisionChannel::ECC_Pawn), FCollisionShape::MakeBox(FVector(spySightRadius, spySightRadius, 1)));
		overlappingResults = overlappingResults.FilterByPredicate([](const FOverlapResult& result) {
			return result.Actor->ActorHasTag("enemy");
		});
		std::vector<webstructs::GaurdData> gaurdDatas;
		for (FOverlapResult result : overlappingResults) {
			FVector gaurdLocation = result.GetActor()->GetActorLocation();

			FHitResult outHit;
			GetWorld()->LineTraceSingleByChannel(outHit, spyLocation, gaurdLocation, ECollisionChannel::ECC_Visibility);
			if (!outHit.IsValidBlockingHit()) {
				//Distance
				float distance = (gaurdLocation - spyLocation).Size2D();
				//Direction
				FVector forwardVector = spyCharacter->GetActorForwardVector(); forwardVector.Z = 0;
				FVector gaurdDirection = (gaurdLocation - spyLocation); gaurdDirection.Z = 0;
				FRotator rotationBetweenVectors = UKismetMathLibrary::FindLookAtRotation(gaurdDirection, forwardVector);
				std::string direction = angleToDirection(rotationBetweenVectors.Yaw);
				//Gaurd Datas
				gaurdDatas.push_back(webstructs::GaurdData(direction, distance));
			}
		}
		json j;
		j["type"] = "success";
		j["num_guards"] = gaurdDatas.size();
		response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	}
	else if (question_type == "surroundings_question") {
		UE_LOG(LogTemp, Warning, TEXT("Inside Surroundings Question"));
		FVector spyPosition = spyCharacter->GetActorLocation();

		TArray<AActor*> gameObjects;
		std::vector<std::string> gameObjectNamesWithinRadius;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("pickupable"), gameObjects);
		for (AActor* gameObject : gameObjects) {
			FVector gameObjectLocation = gameObject->GetActorLocation();
			float distance = (spyPosition - gameObjectLocation).Size2D();
			if (distance < spySightRadius) {
				for (FName tag : gameObject->Tags) {
					if (tag.IsEqual(FName("pickupable"))) continue;
					else {
						std::string tagString = std::string(TCHAR_TO_UTF8(*tag.ToString()));
						gameObjectNamesWithinRadius.push_back(tagString);
					}
				}
			}
		}
		//Camera, Guards, Pickupables
		json j;
		j["type"] = "success";
		j["surroundings"] = json(gameObjectNamesWithinRadius);
		response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	}
	else if (question_type == "see_object_question") {
		UE_LOG(LogTemp, Warning, TEXT("Inside See Objects Question"));
		std::string object_name = questionJSON["object_name"].get<std::string>();

		TArray<FOverlapResult> overlappingResults;
		FVector spyPosition = spyCharacter->GetActorLocation();
		bool found = false;

		TArray<AActor*> gameObjects;
		std::vector<std::string> gameObjectNamesWithinRadius;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("Game Object"), gameObjects);
		for (AActor* gameObject : gameObjects) {
			FVector gameObjectLocation = gameObject->GetActorLocation();
			float distance = (spyPosition - gameObjectLocation).Size2D();
			if (distance < spySightRadius) {
				for (FName tag : gameObject->Tags) {
					if (tag.IsEqual(FName(object_name.c_str()))) {
						found = true;
						break;
					}
				}
			}
		}
		json j;
		j["type"] = found ? "success" : "failure";
		response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	}
	else if (question_type == "timer_question") {
		UE_LOG(LogTemp, Warning, TEXT("Inside Timer Question"));
		int elapsedTime = spyGameMode->getElapsedTime();
		json j;
		j["type"] = "success";
		j["mins_remaining"] = elapsedTime;
		response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Inside Default Branch"));
		json j;
		j["type"] = "failure";
		response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	}
	response->SetResponseContentType(EMediaType::APPLICATION_JSON);
	return response;
}

UResponse* AWebHandlerActor::getTerminalLocation()
{

	FVector terminalLocation = spyGameMode->currentObjective->GetActorLocation();
	webstructs::Vector2D terminalLocation2D = webstructs::Vector2D(terminalLocation.X, terminalLocation.Y);
	json j;
	j["target_location"] = terminalLocation2D;

	UResponse* response = UResponse::ConstructResponse(GetWorld());
	response->SetResponseContent(FString(j.dump().c_str()));
	response->SetResponseStatusCode(EHttpStatusCode(OK));
	return response;
}


//terminal-result
//  === Minigames (Start) =====================================================================================

#include "Character/SpyController.h"
UResponse* AWebHandlerActor::hackingTerminalChallengeResult(UConnection* connection)
{
	FString resultJSONFString = connection->GetData();
	FString contentLengthFString = connection->GetHeader(FString("Content-Length"));
	int contentLength = std::stoi(std::string(TCHAR_TO_UTF8(*contentLengthFString)));
	std::string resultJSONString = std::string(TCHAR_TO_UTF8(*resultJSONFString)).substr(0, contentLength);
	json resultJSON = json::parse(resultJSONString);
	bool result = resultJSON["result"].get<bool>();

	UE_LOG(LogTemp, Warning, TEXT("RESULT[%s]"), result ? TEXT("TRUE") : TEXT("FALSE"));
	if (result) {
		spyGameMode->setNextTerminal();
		ASpy* spyCharacter = getSpyCharacter();
		if (IsValid(spyCharacter)) {
			ASpyController* spyController = Cast<ASpyController>(spyCharacter->GetController());
			spyController->stopMinigame();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Hacking Terminal Finish: Could Not Find spyCharacter"));
		}
	}

	UResponse* response = UResponse::ConstructResponse(GetWorld());
	response->SetResponseContentType(EMediaType::APPLICATION_JSON);
	response->SetResponseStatusCode(EHttpStatusCode(200));
	return response;
}

//minigame-start
UResponse* AWebHandlerActor::getshouldMinigameStart()
{
	json j;
	j["start"] = shouldMinigameStart;
	UE_LOG(LogTemp, Warning, TEXT("SHOULD_MINIGAME_START[%s]"), shouldMinigameStart ? TEXT("True") : TEXT("False"));
	UResponse* response = UResponse::ConstructResponseExt(GetWorld(), FString(j.dump().c_str()));
	response->SetResponseContentType(EMediaType::APPLICATION_JSON);
	return response;
}


ASpy* AWebHandlerActor::getSpyCharacter()
{
	if (IsValid(spyCharacter)) {
		return spyCharacter;
	}
	else {
		TArray<AActor*> outActors;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("SpyCharacter"), outActors);
		if (outActors.IsValidIndex(0)) {
			spyCharacter = Cast<ASpy>(outActors[0]);
			return spyCharacter;
		}
		else {
			return nullptr;
		}
	}
}



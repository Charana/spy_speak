// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Pickupable.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "UObject/ConstructorHelpers.h"
#include <unordered_map>
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

UCLASS()
class SPY_SPEAK_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(BlueprintReadWrite)
	bool knockedOut = false;
	UPROPERTY(BlueprintReadWrite)
	bool beingStrangled = false;
	UPROPERTY(BlueprintReadWrite)
	bool isFighting = false;
	UFUNCTION(BlueprintImplementableEvent)
	void setIsDead();
	UFUNCTION(BlueprintImplementableEvent)
	void StopGuardMovement();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<PickupType::Type> gaurdPocketItemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* gaurdPocketComp;

	float detectionRadius = 15000.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	std::unordered_map<PickupType::Type, UStaticMesh*> typeToStaticMesh;

};

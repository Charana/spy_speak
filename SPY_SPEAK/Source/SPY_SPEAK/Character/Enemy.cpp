// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"


// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	gaurdPocketComp = CreateDefaultSubobject<UStaticMeshComponent>("GaurdPocket");
	gaurdPocketComp->AttachTo(GetMesh(), FName("GaurdPocket"), EAttachLocation::SnapToTargetIncludingScale, true);
	gaurdPocketComp->SetMobility(EComponentMobility::Movable);
	gaurdPocketComp->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
	
	typeToStaticMesh[PickupType::Type::NONE] = nullptr;
	ConstructorHelpers::FObjectFinder<UStaticMesh> RockSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/RockPickupSM"));
	typeToStaticMesh[PickupType::Type::ROCK] = RockSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> MugSM(TEXT("/Game/Charana/BPs/Pickupable/Destructables/MugPickupSM"));
	typeToStaticMesh[PickupType::Type::MUG] = MugSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> BeakerSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/BeakerPickupSM"));
	typeToStaticMesh[PickupType::Type::BEAKER] = BeakerSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> CoffeeCupSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/CoffeeCupPickupSM"));
	typeToStaticMesh[PickupType::Type::COFFEE_CUP] = CoffeeCupSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> BottleSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/BottlePickupSM"));
	typeToStaticMesh[PickupType::Type::BOTTLE] = BottleSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> CanSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/CanPickupSM"));
	typeToStaticMesh[PickupType::Type::CAN] = CanSM.Object;

	Tags.Add(FName("gaurd"));
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	gaurdPocketComp->SetStaticMesh(typeToStaticMesh[gaurdPocketItemType.GetValue()]);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.

#include "SpyController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "AI/Navigation/NavigationPath.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "ActionHandler.h"
#include "Engine/Engine.h"
#include "Extras/OpenData.h"
#include "Extras/CompositeData.h"
#include "Extras/StairsData.h"
#include "Extras/ChangeStanceData.h"
#include "Extras/HackCameraData.h"
#include "Extras/DropData.h"
#include "Extras/TransformData.h"
#include "Extras/StopGuardMovementData.h"
#include "Extras/HideData.h"
#include "Actors/Openable.h"
#include "Actors/Pickupable.h"
#include "Extras/RelativeTransformData.h"
#include "UObject/ConstructorHelpers.h"
#include "Perception/AISense_Hearing.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CommandBNF/Actions/Speed.h"
#include "SpyGameModeBase.h"
#include "Cameras/Camera.h"


ASpyController::ASpyController() {
	Tags.Add(FName("SpyController"));
	UE_LOG(LogTemp, Warning, TEXT("ASpyController Constructor"));
	
	typeToClass[PickupType::Type::NONE] = nullptr; 
	ConstructorHelpers::FClassFinder<APickupable> RockPickup(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/RockPickupBP"));
	typeToClass[PickupType::Type::ROCK] = RockPickup.Class;
	ConstructorHelpers::FClassFinder<APickupable> MugPickup(TEXT("/Game/Charana/BPs/Pickupable/Destructables/MugPickupBP"));
	typeToClass[PickupType::Type::MUG] = MugPickup.Class;
	ConstructorHelpers::FClassFinder<APickupable> BeakerPickup(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/BeakerPickupBP"));
	typeToClass[PickupType::Type::BEAKER] = BeakerPickup.Class;
	ConstructorHelpers::FClassFinder<APickupable> CoffeeCupPickup(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/CoffeeCupPickupBP"));
	typeToClass[PickupType::Type::COFFEE_CUP] = CoffeeCupPickup.Class;
	ConstructorHelpers::FClassFinder<APickupable> BottlePickup(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/BottlePickupBP"));
	typeToClass[PickupType::Type::BOTTLE] = BottlePickup.Class;
	ConstructorHelpers::FClassFinder<APickupable> CanPickup(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/CanPickupBP"));
	typeToClass[PickupType::Type::CAN] = CanPickup.Class;
	
	typeToStaticMesh[PickupType::Type::NONE] = nullptr;
	ConstructorHelpers::FObjectFinder<UStaticMesh> RockSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/RockPickupSM"));
	typeToStaticMesh[PickupType::Type::ROCK] = RockSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> MugSM(TEXT("/Game/Charana/BPs/Pickupable/Destructables/MugPickupSM"));
	typeToStaticMesh[PickupType::Type::MUG] = MugSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> BeakerSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/BeakerPickupSM"));
	typeToStaticMesh[PickupType::Type::BEAKER] = BeakerSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> CoffeeCupSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/CoffeeCupPickupSM"));
	typeToStaticMesh[PickupType::Type::COFFEE_CUP] = CoffeeCupSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> BottleSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/BottlePickupSM"));
	typeToStaticMesh[PickupType::Type::BOTTLE] = BottleSM.Object;
	ConstructorHelpers::FObjectFinder<UStaticMesh> CanSM(TEXT("/Game/Charana/BPs/Pickupable/Non-Destructables/CanPickupSM"));
	typeToStaticMesh[PickupType::Type::CAN] = CanSM.Object;

	typeToString[PickupType::Type::NONE] = "none";
	typeToString[PickupType::Type::ROCK] = "rock";
	typeToString[PickupType::Type::MUG] = "mug";
	typeToString[PickupType::Type::BEAKER] = "beaker";
	typeToString[PickupType::Type::COFFEE_CUP] = "coffee cup";
	typeToString[PickupType::Type::BOTTLE] = "bottle";
	typeToString[PickupType::Type::CAN] = "can";

	currentItem = Item();
	//currentItem = Item(PickupType::ROCK);
}

void ASpyController::Possess(APawn* pawn) {
	Super::Possess(pawn);
	character = Cast<ASpy>(pawn);
	if(pawn == nullptr) UE_LOG(LogTemp, Warning, TEXT("SpyController:: ASpy never possessed by SpyController (Must be possessed some other way)"));
}


void ASpyController::Tick(float DeltaTime)
{
	if (character->GetVelocity().Size() > 10) UAISense_Hearing::ReportNoiseEvent(GetWorld(), character->GetActorLocation(), 1.0f, character);
}

#include "AI/Navigation/RecastNavMesh.h"
void ASpyController::BeginPlay() {
	Super::BeginPlay();
	//Possess(character);

	navSys= GetWorld()->GetNavigationSystem();
	navSys->GetNavigationBounds();

	TArray<AActor*> outActors1;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ARecastNavMesh::StaticClass(), outActors1);
	if (outActors1.IsValidIndex(0)) {
		Cast<ARecastNavMesh>(outActors1[0])->AgentHeight = 2640;
		Cast<ARecastNavMesh>(outActors1[0])->AgentMaxHeight = 2640;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("NO RECAST NAVIGATION DATA"));
	}

	UE_LOG(LogTemp, Warning, TEXT("SpyController: Spawning ActionHandler"))
	actionHandler = Cast<AActionHandler>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), AActionHandler::StaticClass(), FTransform()));
	if (actionHandler != nullptr) {
		actionHandler->InitController(this);
		UGameplayStatics::FinishSpawningActor(actionHandler, FTransform());
	}


	TArray<AActor*> outActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWebHandlerActor::StaticClass(), outActors);
	if (outActors.IsValidIndex(0)) {
		webHandler = Cast<AWebHandlerActor>(outActors[0]);
		webHandler->currentItem = typeToString[PickupType::NONE];
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("WebHandler cannot be found"));
	}

	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), )
	//TArray<AActor*> Cameras;
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACamera::StaticClass(), Cameras);

	//for (int i = 0; i < cameraControllersNum; i++) {
	//	APlayerController* cameraController = UGameplayStatics::CreatePlayer(GetWorld());
	//	cameraControllers.Add(cameraController);
	//	cameraControllers[i]->GetPawn()->Destroy();
	//	if (Cameras.IsValidIndex(i)) {
	//		ACamera* cameraBody = Cast<ACamera>(Cameras[i]);
	//		cameraControllers[i]->Possess(cameraBody);
	//	}
	//}

}

void ASpyController::nextDelayedSubAction() {
	UE_LOG(LogTemp, Warning, TEXT("Next Sub Action"));
	if (!queuedSubActions.empty()) {
		Data* subAction = queuedSubActions.front();
		queuedSubActions.pop();
		PlaySubMove(subAction);
	}
	else actionHandler->nextAction();
}

void ASpyController::startMinigame(ATerminal* terminal)
{
	character->shouldHackTerminal = true;
	currentHackingTerminal = terminal;
	//hackTerminalData->terminal->startAudioAndVideo();
	webHandler->shouldMinigameStart = true;
}

void ASpyController::stopMinigame()
{
	character->shouldHackTerminal = false;
	//hackTerminalData->terminal->stopAudioAndVideo();
	UE_LOG(LogTemp, Warning, TEXT("Stopped Audio and Video"));
	webHandler->shouldMinigameStart = false;
}

#include "Kismet/KismetMathLibrary.h"
void ASpyController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	UE_LOG(LogTemp, Warning, TEXT("Move Completed"));
	if (Result == EPathFollowingResult::Aborted) UE_LOG(LogTemp, Warning, TEXT("Move Completed STATUS [Aborted]"));
	if (Result == EPathFollowingResult::Blocked) UE_LOG(LogTemp, Warning, TEXT("Move Completed STATUS [Blocked]"));
	if (Result == EPathFollowingResult::OffPath) UE_LOG(LogTemp, Warning, TEXT("Move Completed STATUS [OffPath]"));
	if (Result == EPathFollowingResult::Invalid) UE_LOG(LogTemp, Warning, TEXT("Move Completed STATUS [Invalid]"));
	if (Result != EPathFollowingResult::Success) {
		return;
	}


	//if (Result == EPathFollowingRequestResult::RequestSuccessful) { UE_LOG(LogTemp, Warning, TEXT("Movement Ruqest Successful")); }
	//if (Result == EPathFollowingRequestResult::AlreadyAtGoal) { UE_LOG(LogTemp, Warning, TEXT("Movement Aborted: Already at goal")); }
	//if (Result == EPathFollowingRequestResult::Failed) { UE_LOG(LogTemp, Warning, TEXT("Movement Aborted: Request Failed")); }

	//TArray<FOverlapResult> overlappingResults;
	//FVector spysLocation = this->GetPawn()->GetActorLocation();
	//GetWorld()->OverlapMultiByObjectType(overlappingResults, spysLocation, FQuat(), FCollisionObjectQueryParams(ECollisionChannel::ECC_Pawn), FCollisionShape::MakeSphere(10000.0f));
	//overlappingResults = overlappingResults.FilterByPredicate([](const FOverlapResult& result) { return result.Actor->ActorHasTag("Camera"); });
	//overlappingResults.Sort([&spysLocation](const FOverlapResult& actor1, const FOverlapResult& actor2) {
	//	return (actor1.GetActor()->GetActorLocation() - spysLocation).Size() < (actor2.GetActor()->GetActorLocation() - spysLocation).Size();
	//});
	//for (int i = 0; i < overlappingResults.Num(); i++) {
	//	UE_LOG(LogTemp, Warning, TEXT("overlapping camera"));
	//	APawn* cameraActor = Cast<APawn>(overlappingResults[i].GetActor());
	//	int j; for (j = 0; j < cameraControllersNum; j++) if (cameraControllers[j]->GetPawn() == cameraActor)  break;
	//	if (j != cameraControllersNum) continue;
	//	cameraControllers[i]->Possess(cameraActor);
	//}
	//UE_LOG(LogTemp, Warning, TEXT("finished switchign to overlapping camera"));

	if (Result == EPathFollowingResult::Success) {
		UE_LOG(LogTemp, Warning, TEXT("Move Completed STATUS [Success]"));
		if (faceDirection != FVector::ZeroVector) {
			FVector2D actorForwardVectorNormalized2D = FVector2D(character->GetActorForwardVector().GetSafeNormal2D());
			UE_LOG(LogTemp, Warning, TEXT("actorForwardVector[%s]"), *actorForwardVectorNormalized2D.ToString());
			FVector2D faceDirectionNormalized2D = FVector2D(this->faceDirection.GetSafeNormal2D());


			//float dotProduct = UKismetMathLibrary::DotProduct2D(actorForwardVectorNormalized2D, faceDirectionNormalized2D);
			//float rotationToFaceDirection = UKismetMathLibrary::DegAcos(dotProduct);
			//UE_LOG(LogTemp, Warning, TEXT("dotProduct[%f]"), dotProduct);
			float degA = std::atan2(actorForwardVectorNormalized2D.Y, actorForwardVectorNormalized2D.X) * 180 / PI;
			float degB = std::atan2(faceDirectionNormalized2D.Y, faceDirectionNormalized2D.X) * 180 / PI;
			float rotationToFaceDirection = degB - degA;


			FRotator newSpyRotation = character->GetActorRotation();
			UE_LOG(LogTemp, Warning, TEXT("oldSpyYaw[%f]"), newSpyRotation.Yaw);
			UE_LOG(LogTemp, Warning, TEXT("Rotation to face direction[%f]"), rotationToFaceDirection);
			newSpyRotation.Yaw = newSpyRotation.Yaw + rotationToFaceDirection;
			UE_LOG(LogTemp, Warning, TEXT("newSpyYaw[%f]"), newSpyRotation.Yaw);

			character->SetActorRotation(newSpyRotation);
		}
		UE_LOG(LogTemp, Warning, TEXT("OnMoveCompleted:nextSubAction"));
		nextDelayedSubAction();
	}
}

void ASpyController::QueueAction(CompositeData* subActions) {
	queuedSubActions = std::queue<Data*>();
	for (Data* subMove : subActions->actionDataList) queuedSubActions.push(subMove);
	nextDelayedSubAction();
}

//TEMPLATE Load Obj From Path
template <typename ObjClass>
static FORCEINLINE ObjClass* LoadObjFromPath(const FName& Path)
{
	if (Path == NAME_None) return NULL;
	return Cast<ObjClass>(StaticLoadObject(ObjClass::StaticClass(), NULL, *Path.ToString()));
}

void ASpyController::dropItem() {
	FTransform pickupSocketTransform = character->GetMesh()->GetSocketTransform(FName("Pickup"), ERelativeTransformSpace::RTS_World);
	FVector spawnLocation = pickupSocketTransform.GetLocation() - character->GetActorForwardVector() * 200 - character->GetActorUpVector() * 30;
	FRotator spawnRotation = pickupSocketTransform.GetRotation().Rotator();
	FVector spawnScale = FVector::OneVector;
	GetWorld()->SpawnActor(typeToClass[currentItem.type], &spawnLocation, &spawnRotation);

	character->PickupStaticMesh->SetStaticMesh(typeToStaticMesh[PickupType::NONE]);
}


#include "Extras/SnapRotationData.h"
#include "BrainComponent.h"
void ASpyController::PlaySubMove(Data* command) {
	UE_LOG(LogTemp, Warning, TEXT("Action Begin"));

	//Set current sub action
	currentSubAction = command;

	if (command->dataType == Data::DataType::STOP_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Stop Begin"));
		StopMovement();
		if (character->shouldHackTerminal) {
			stopMinigame();
		}
		else if (character->shouldHackCameras || character->shouldLeftHide || character->shouldRightHide) {
			//Animmation blueprint transition callback for next subaction
			character->shouldLeftHide = false;
			character->shouldRightHide = false;
			character->shouldHackCameras = false;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("StopData:nextSubAction"));
			nextDelayedSubAction();
		}
	}
	else if (command->dataType == Data::DataType::TRANSFORM_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Transformation Begin"));
		TransformData* transformData = static_cast<TransformData*>(command);
		if(transformData->location != nullptr) character->SetActorLocation(*transformData->location);
		if(transformData->rotation != nullptr) character->SetActorRotation(*transformData->rotation);
		UE_LOG(LogTemp, Warning, TEXT("TransformData:nextSubAction"));
		nextDelayedSubAction();
	}
	else if (command->dataType == Data::DataType::RELATIVE_TRANSFORM_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Relative Transformation Begin"));
		RelativeTransformData* transformData = static_cast<RelativeTransformData*>(command);
		character->AddActorLocalOffset(transformData->location);

		FRotator relativeRotation = character->GetActorRotation() + transformData->rotation;
		character->SetActorRotation(relativeRotation);
		UE_LOG(LogTemp, Warning, TEXT("RelativeTransformData:nextSubAction"));
		nextDelayedSubAction();
	}
	else if (command->dataType == Data::DataType::TURN_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Turn Transformation Begin"));
		turnData = static_cast<TurnData*>(command);
		if (turnData->direction == Turn::Direction::LEFT) character->shouldTurnLeft = true;
		if (turnData->direction == Turn::Direction::RIGHT) character->shouldTurnRight = true;
	}
	else if (command->dataType == Data::DataType::SNAP_ROTATION) {
		UE_LOG(LogTemp, Warning, TEXT("Snap Rotation Begin"));
		SnapRotationData* turnData = static_cast<SnapRotationData*>(command);

		float Yaw = round((character->GetActorRotation() + turnData->rotation).Yaw / 90) * 90;
		FRotator absoluteRotateData(turnData->rotation.Pitch, Yaw, turnData->rotation.Roll);
		character->SetActorRotation(absoluteRotateData);
		UE_LOG(LogTemp, Warning, TEXT("SnapRotation:nextSubAction"));
		nextDelayedSubAction();
	}
	else if (command->dataType == Data::DataType::MOVE_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Movement Begin"));
		MoveData* moveData = static_cast<MoveData*>(command);
		switch (moveData->speed) {
		case Speed::WALK:
			character->GetCharacterMovement()->MaxWalkSpeed = 9000 / 4;
			break;
		case Speed::RUN:
			character->GetCharacterMovement()->MaxWalkSpeed = 9000;
			break;
		}
		this->faceDirection = moveData->faceDirection;
		EPathFollowingRequestResult::Type result = MoveToLocation(moveData->dest, 1000.0f);
		if (result == EPathFollowingRequestResult::RequestSuccessful) { UE_LOG(LogTemp, Warning, TEXT("Movement Ruqest Successful")); }
		if (result == EPathFollowingRequestResult::AlreadyAtGoal) { UE_LOG(LogTemp, Warning, TEXT("Movement Aborted: Already at goal")); }
		if (result == EPathFollowingRequestResult::Failed) { UE_LOG(LogTemp, Warning, TEXT("Movement Aborted: Request Failed")); }
	}
	else if (command->dataType == Data::DataType::OPEN_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Opening Door Begin"));
		OpenData* openData = static_cast<OpenData*>(command);
		if (openData->openDirection == OpenData::OpenDirection::INWARD) {
			character->startInwardOpenDoorMontage();
			openData->door->startDoorOpenInwardMontage();
		} else {
			character->startOutwardOpenDoorMontage();
			openData->door->startDoorOpenOutwardMontage();
		}
	}
	else if (command->dataType == Data::DataType::DROP_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Drop Begin"));
		DropData* dropData = static_cast<DropData*>(command);

		//Drop current held object (if there is one) on the floor
		if (currentItem.set == true) dropItem();
		//Remove item
		currentItem = Item();

		UE_LOG(LogTemp, Warning, TEXT("DropData:nextSubAction"));
		nextDelayedSubAction();
	}
	else if (command->dataType == Data::DataType::PICKUP_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Pickup Begin"));
		pickupData = static_cast<PickupData*>(command);

		//Drop current held object (if there is one) on the floor
		if (currentItem.set == true) dropItem();

		//Start pickup animation
		if (pickupData->level == PickupData::PickupLevel::FLOOR) character->shouldFloorPickup = true;
		else if (pickupData->level == PickupData::PickupLevel::WAIST) character->shouldWaistPickup = true;
	}
	else if (command->dataType == Data::DataType::PICKPOCKET_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Pickpocket Begin"));
		pickpocketData = static_cast<PickpocketData*>(command);

		if (currentItem.set == true) dropItem();

		//Start pickup animation
		character->shouldPickpocket = true;
	}
	else if (command->dataType == Data::DataType::THROW_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Throw Begin"));
		ThrowData* throwData = static_cast<ThrowData*>(command);
		throwDest = throwData->throwLocation;
		throwElevation = throwData->throwElevation;

		//Start Throw animations
		character->shouldThrow = true;
	}
	else if (command->dataType == Data::DataType::STOP_GUARD_MOVEMENT_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Stop Guard Movement Begin"));
		StopGuardMovementData* stopGuardMovementData = static_cast<StopGuardMovementData*>(command);
		stopGuardMovementData->enemyGaurd->StopGuardMovement();
		nextDelayedSubAction();
	}
	else if (command->dataType == Data::DataType::STRANGLE_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Strangle Begin"));
		strangleData = static_cast<StrangleData*>(command);
		StrangleDisplacement = strangleData->forwardDisplacement;
		StrangleGaurd = strangleData->enemyGaurd;
		
		//Animation Blueprint -
			// On Strangle - Enter Guard Strangle State
			// On Strangle Finish - Enter Guard Dead State
			// On Strangle Abort - Exit Guard Strangle State

		StrangleGaurd->StopGuardMovement();
		//Set Animations
		character->isStrangling = true;
		StrangleGaurd->beingStrangled = true;
	}
	//else if (command->dataType == Data::DataType::STAIRS_DATA) {
	//	UE_LOG(LogTemp, Warning, TEXT("Stairs Begin"));
	//	StairsData* stairsData = static_cast<StairsData*>(command);
	//	FName destination = stairsData->stairs->destination;

	//	ASpyGameModeBase* gameMode = Cast<ASpyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	//	if (destination.IsEqual(FName("Basement"))) gameMode->Basement();
	//	if (destination.IsEqual(FName("First Floor"))) gameMode->FirstFloor();
	//	nextDelayedSubAction();
	//}
	else if (command->dataType == Data::DataType::CHANGE_STANCE_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Change Stance Begin"));
		//Change crouch/standup booleans to change animation from idle/walk/run
		//The these booleans set on the SpyCharacter ? which can be accessed from AnimationBlueprint
		ChangeStanceData* changeStanceData = static_cast<ChangeStanceData*>(command);
		UE_LOG(LogTemp, Warning, TEXT("type: %s"), character->shouldStand ? TEXT("True") : TEXT("False"));
		UE_LOG(LogTemp, Warning, TEXT("type: %d"), (int) changeStanceData->stance);
		if (changeStanceData->stance == Stance::STAND && character->shouldStand != true) {
			character->shouldStand = true;
			UE_LOG(LogTemp, Warning, TEXT("Stand"));
		}
		else if (changeStanceData->stance == Stance::CROUCH && character->shouldStand == true) {
			character->shouldStand = false;
			UE_LOG(LogTemp, Warning, TEXT("Crouch"));
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Not Stand or Crouch, Calling next subAction"));
			nextDelayedSubAction();
		}
	}
	else if (command->dataType == Data::DataType::HIDE_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Hide Begin"));
		HideData* hideData = static_cast<HideData*>(command);
		if(hideData->faceDirection == HideData::FaceDirection::FACE_LEFT) character->shouldLeftHide = true;
		if (hideData->faceDirection == HideData::FaceDirection::FACE_RIGHT) character->shouldRightHide = true;
		UE_LOG(LogTemp, Warning, TEXT("HideData:nextSubAction"));
		nextDelayedSubAction();
	}
	else if (command->dataType == Data::DataType::HACK_CAMERA_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Hack Camera"));
		HackCameraData* hackCameraData = static_cast<HackCameraData*>(command);
		character->shouldHackCameras = true;
		webHandler->shouldMinigameStart = true;
	}
	else if (command->dataType == Data::DataType::HACK_TERMINAL_DATA) {
		UE_LOG(LogTemp, Warning, TEXT("Hack Terminal"));
		hackTerminalData = static_cast<HackTerminalData*>(command);
		ATerminal* terminal = Cast<ATerminal>(hackTerminalData->terminal);
		startMinigame(terminal);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Not any known action"));
		//UE_LOG(LogTemp, Warning, TEXT("type: %d"), (int) command->dataType);
	}
		
	//FAIRequestID currentMoveID = GetCurrentMoveRequestID();
	//navSys->SimpleMoveToLocation(this, movementDest);

	//TArray<AActor*> outActors;
	//UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("Room 812"), outActors);
	//MoveToActor(outActors[0]);
	//navSys->SimpleMoveToActor(this, outActors[0]);

	////Navigation Path from start to dest relative to character actor
	//UNavigationPath* upath = navSys->FindPathToLocationSynchronously(GetWorld(), character->GetActorLocation(), movementDest, character);
	//if (upath && upath->IsValid()) {
	//	FAIMoveRequest moveRequest = FAIMoveRequest(movementDest);
	//	moveRequest.SetUsePathfinding(true);
	//	moveRequest.SetAllowPartialPath(true);
	//	moveRequest.SetAcceptanceRadius(50.0f);

	//	//Request Movement to Location (Requests can be paused, stopped, resumed, status queried etc.)
	//	FAIRequestID moveID = RequestMove(moveRequest, upath->GetPath());
	//}
}

void ASpyController::conditionallyInterruptStrangle() {
	if (currentSubAction->dataType == Data::DataType::STRANGLE_DATA) {
		strangleData->enemyGaurd->beingStrangled = false;
	}
}


void ASpyController::throwPickupableProjectile() {
	//currentItem = Item(PickupType::ROCK);

	//Remove held item
	character->PickupStaticMesh->SetStaticMesh(typeToStaticMesh[PickupType::NONE]);

	//============== CALCULATE THROW PARAMETERS =================================================================================================
	//Calc Direction (Normalized) Vector
	FVector DistanceVector3D = throwDest - character->GetActorLocation();
	FVector horizontalDirection = DistanceVector3D.GetSafeNormal2D();
	//Obtain which floor
	float characterDepth = character->GetActorLocation().Z;
	float floorDepth = -1;
	if (characterDepth > 12710.f && characterDepth < 18370.f) floorDepth = 12710.f;		//Basement - 13710.0 to 18370.0
	else if (characterDepth > 19700.f && characterDepth < 25800.f) floorDepth = 19700.f;	//First Floor - 19700.0 to 25800.0
	else if (characterDepth > 25800.f) floorDepth = 25800.f; 					//Roof - 25800.0 upwards
	//Calc Time Taken to Hit Ground (Vectical)
	//Time t taken for an object to fall distance d <==>  t=sqrt(2d/g)
	FVector startLocation = character->GetMesh()->GetSocketLocation(FName("Pickup"));
	UE_LOG(LogTemp, Warning, TEXT("SOCKET_Z[%f]"), startLocation.Z);
	UE_LOG(LogTemp, Warning, TEXT("FLOOR_Z[%f]"), floorDepth);
	float DistanceFromFloor = startLocation.Z - (floorDepth + throwElevation);
	UE_LOG(LogTemp, Warning, TEXT("DistanceFromFloor[%f]"), DistanceFromFloor);
	float gravity = -GetWorld()->GetGravityZ();
	UE_LOG(LogTemp, Warning, TEXT("gravity[%f]"), gravity);
	float timeTaken = std::sqrt((2 * DistanceFromFloor) / gravity);
	//Calc initialSpeed to Move to DestActor based on TimeTaken (assume constant horizontal velocity)
	float horizontalDistance = DistanceVector3D.Size2D();
	UE_LOG(LogTemp, Warning, TEXT("horizontalDistance[%f]"), horizontalDistance);
	float initialHorizontalSpeed = horizontalDistance / timeTaken;

	//==========================================================================================================================================

	//Throw Pickup Projectile
	FTransform pickupSocketTransform = character->GetMesh()->GetSocketTransform(FName("Pickup"), ERelativeTransformSpace::RTS_World);
	pickupSocketTransform.SetScale3D(FVector::OneVector);
	APickupable* throwable = Cast<APickupable>(GetWorld()->SpawnActor(typeToClass[currentItem.type], &pickupSocketTransform));
	throwable->setProjectileMovementParameters(horizontalDirection, initialHorizontalSpeed);
	UE_LOG(LogTemp, Warning, TEXT("horizontalDirection[%s]"), *horizontalDirection.ToString());
	UE_LOG(LogTemp, Warning, TEXT("initialHorizontalSpeed[%f]"), initialHorizontalSpeed);

	//Remove item
	currentItem = Item();
}

void ASpyController::pickupPickupableProjectile() {
	//Pickup chosen object
	currentItem = Item(pickupData->actor->type);
	pickupData->actor->pickup();
	//Hold item
	character->PickupStaticMesh->SetStaticMesh(typeToStaticMesh[pickupData->actor->type]);
	//Notify webhandler
	webHandler->currentItem = typeToString[pickupData->actor->type];
}

void ASpyController::pickpocketPickupableProjectile() {
	//Pickup chosen object
	currentItem = Item(pickpocketData->enemyGaurd->gaurdPocketItemType);
	//Hold item
	character->PickupStaticMesh->SetStaticMesh(typeToStaticMesh[pickpocketData->enemyGaurd->gaurdPocketItemType]);
	//Notify webhandler
	webHandler->currentItem = typeToString[pickpocketData->enemyGaurd->gaurdPocketItemType];
}

void ASpyController::turnSpy()
{
	FRotator rotation = character->GetActorRotation();
	rotation.Yaw = turnData->yaw;
	character->SetActorRotation(rotation);
}

void ASpyController::killGuard() {
	strangleData->enemyGaurd->setIsDead();
}




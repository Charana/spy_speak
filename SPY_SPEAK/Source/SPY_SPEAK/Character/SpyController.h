// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Spy.h"
#include "Extras/Either.h"
#include "Extras/MoveData.h"
#include "Extras/PickupData.h"
#include "Extras/ThrowData.h"
#include "Extras/TurnData.h"
#include "Extras/StrangleData.h"
#include "Extras/PickpocketData.h"
#include "Extras/HackTerminalData.h"
#include "CommandBNF/Actions/Stop.h"
#include <queue>
#include <deque>
#include <unordered_map>
#include "Actors/Pickupable.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "WebHandlerActor.h"
#include "SpyController.generated.h"

UCLASS()
class SPY_SPEAK_API ASpyController : public AAIController
{
	GENERATED_BODY()

public:
	ASpyController();
	void PlaySubMove(Data* location);
	void QueueAction(CompositeData* subActions);

	
	std::queue<Data*> queuedSubActions;
	Data* currentSubAction;

	
	UFUNCTION(BlueprintCallable)
	void nextDelayedSubAction();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AWebHandlerActor* webHandler;

	UFUNCTION(BlueprintCallable)
	void startMinigame(ATerminal* terminal);

	UFUNCTION(BlueprintCallable)
	void stopMinigame();

	UPROPERTY(BlueprintReadWrite)
	ATerminal* currentHackingTerminal = nullptr;

	UFUNCTION(BlueprintCallable)
	void conditionallyInterruptStrangle();




	//Pickup Stuff
	PickupData* pickupData;
	//Throwing Stuff
	FVector throwDest;
	float throwElevation;
	//Pickpocket Stuff
	PickpocketData* pickpocketData;
	//Turn Stuff
	TurnData* turnData;
	//Strangle Stuff
	UPROPERTY(BlueprintReadWrite)
	FVector StrangleDisplacement;
	UPROPERTY(BlueprintReadWrite)
	AEnemy* StrangleGaurd;
	//Hack Terminal Stuff
	HackTerminalData* hackTerminalData;
	//Strangle Stuff
	StrangleData* strangleData;

	UFUNCTION(BlueprintCallable)
	void throwPickupableProjectile();
	UFUNCTION(BlueprintCallable)
	void pickupPickupableProjectile();
	UFUNCTION(BlueprintCallable)
	void pickpocketPickupableProjectile();
	UFUNCTION(BlueprintCallable)
	void turnSpy();
	UFUNCTION(BlueprintCallable)
	void killGuard();

	void dropItem();

	//Item Stuff
	class Item {
	public:
		bool set;
		PickupType::Type type;
		Item() : set(false) {};
		Item(PickupType::Type type) : type(type), set(true) {};
	};
	Item currentItem;

protected:
	std::unordered_map<PickupType::Type, TSubclassOf<AActor>> typeToClass;
	std::unordered_map<PickupType::Type, std::string> typeToString;
	//std::unordered_map<PickupType::Type, FName> typeToPath;
	std::unordered_map<PickupType::Type, UStaticMesh*> typeToStaticMesh;


	virtual void BeginPlay() override;
	virtual void Possess(APawn* PossessedPawn) override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;
	bool currentlyMoving = false;
	UNavigationSystem* navSys;
	ASpy* character;
	class AActionHandler* actionHandler;
	int cameraControllersNum = 3;
	TArray<APlayerController*> cameraControllers;

	FVector faceDirection;
};
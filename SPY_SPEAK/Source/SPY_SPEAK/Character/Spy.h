// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/StaticMeshComponent.h"
#include "Spy.generated.h"

UCLASS(config = Game)
class SPY_SPEAK_API ASpy : public ACharacter
{
	GENERATED_BODY()

public:
	ASpy();
	UFUNCTION(BlueprintCallable)
	int getCurrentFloor();
	UFUNCTION(BlueprintCallable)
	FString floorIntToString(int floor_num);
	UFUNCTION(BlueprintCallable)
	float floorIntToHeight(int floor_num);
	UFUNCTION(BlueprintCallable)
	int getFloor(FVector location);
	
	UPROPERTY(BlueprintReadWrite)
	bool shouldStand = true;
	UPROPERTY(BlueprintReadWrite)
	bool shouldFloorPickup = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldWaistPickup = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldThrow = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldLeftHide = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldRightHide = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldHackTerminal = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldHackCameras = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldPickpocket = false;
	UPROPERTY(BlueprintReadWrite)
	bool isStrangling = false;
	UPROPERTY(BlueprintReadWrite)
	bool isFighting = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldTurnRight = false;
	UPROPERTY(BlueprintReadWrite)
	bool shouldTurnLeft = false;

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation Montages")
	void startInwardOpenDoorMontage();
	UFUNCTION(BlueprintImplementableEvent, Category = "Animation Montages")
    void startOutwardOpenDoorMontage();

	//Body Camera
	UPROPERTY(EditAnywhere, Category = "Body Camera")
	float bodyCamRotDelta = 5.0f;

	//Pickup Static Mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* PickupStaticMesh;

	/** Follow camera */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ABodyCamera* BodyCam;

protected:
	double multiplier = 15.0f;
	// APawn interface overriden
	virtual void BeginPlay() override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void Tick(float DeltaTime) override;

	//UPROPERTY(EditAnywhere)
	//class UCameraComponent* FollowCamera;

	//Body Camera
	float prevYaw;

	TSubclassOf<ABodyCamera> bodyCamClass = nullptr;
};


// Fill out your copyright notice in the Description page of Project Settings.

#include "Spy.h"
#include "Engine/GameEngine.h"
#include "Cameras/BodyCamera.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Actors/Rotatable.h"
#include "UObject/ConstructorHelpers.h"
#include "SpyController.h"

//////////////////////////////////////////////////////////////////////////
// AThirdPersonCharacter

ASpy::ASpy()
{
	//Skeleton Mesh and Animations set in blueprint

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f * multiplier;
	GetCharacterMovement()->MaxAcceleration = 2048.0f * multiplier;

	// Pickup Static Mesh Component
	PickupStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Pickup"));
	FName pickupSocket = TEXT("Pickup");
	PickupStaticMesh->AttachTo(GetMesh(), pickupSocket, EAttachLocation::SnapToTargetIncludingScale, true);
	PickupStaticMesh->SetMobility(EComponentMobility::Movable);
	PickupStaticMesh->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
	PickupStaticMesh->SetStaticMesh(nullptr);

	//Create a follow camera
	//FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	//FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	//FollowCamera->bUsePawnControlRotation = true; // Camera does not rotate relative to arm

	Tags.Add(FName("SpyCharacter"));

	ConstructorHelpers::FClassFinder<ABodyCamera> BodyCameraClass(TEXT("/Game/Charana/BPs/BodyCamera_BP"));
	if (BodyCameraClass.Class != nullptr){
		bodyCamClass = BodyCameraClass.Class;
	}

	UE_LOG(LogTemp, Warning, TEXT("ASpy Constructor"));
	//AIControllerClass = ASpyController::StaticClass();
	//AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void ASpy::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("ASpy BeginPlay Started"));

	//Attach a Camera Actor
	if(bodyCamClass != nullptr){
		BodyCam = Cast<ABodyCamera>(GetWorld()->SpawnActor(bodyCamClass));
		BodyCam->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		BodyCam->setBodyCameraRotation();
	}

	//ConstructorHelpers::FObjectFinder<UEnvQuery> hideEQSQuery(TEXT("/Game/Charana/BPs/EQS/HideEQSQuery"));
	//if (hideEQSQuery.Object != nullptr) {
	//	this->hideEQSQuery = hideEQSQuery.Object;
	//	UE_LOG(LogTemp, Warning, TEXT("HIDE EQS QUERY FOUND"));
	//}

	//SpawnDefaultController();
	AController* contrl = GetController();
	if (contrl == nullptr) { UE_LOG(LogTemp, Warning, TEXT("ASpy::BeginPlay:: GetController() is NULL")); }
	if (Cast<APlayerController>(contrl) == nullptr) { UE_LOG(LogTemp, Warning, TEXT("ASpy::BeginPlay::Cast failed, default AI controller not APlayerController")); }
	if (Cast<ASpyController>(contrl) == nullptr) { UE_LOG(LogTemp, Warning, TEXT("Apy::BeginPlay::Cast failed, default AI controller not ASpyController")); }

}

void ASpy::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	UE_LOG(LogTemp, Warning, TEXT("PossessedBy:: Fired"));
	if (Cast<APlayerController>(NewController) == nullptr) { UE_LOG(LogTemp, Warning, TEXT("ASpy::PossessedBy:: Cast failed, default AI controller not APlayerController")); }
	if (Cast<ASpyController>(NewController) == nullptr) { UE_LOG(LogTemp, Warning, TEXT("ASpy::PossessedBy:: Cast failed, default AI controller not ASpyController")); }
}

void ASpy::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	float currentYaw = GetActorRotation().Yaw;
	if (FMath::Abs(currentYaw - prevYaw) > bodyCamRotDelta) {
		BodyCam->setBodyCameraRotation();
	}
	//To check if controller rotation is being updated during MoveToLocation()
	//UE_LOG(LogTemp, Warning, TEXT("Control Rotation: %s"), *GetController()->GetControlRotation().ToString());
	//BodyCam->SetActorRotation(GetActorRotation());
}

int ASpy::getCurrentFloor() {
	return getFloor(GetActorLocation());
}

FString ASpy::floorIntToString(int floor_num)
{
	if (floor_num == 0) return "basement";
	else if (floor_num == 1) return "ground floor";
	else if (floor_num == 2) return "basement";
	else return "none";
}

float ASpy::floorIntToHeight(int floor_num) {
	if (floor_num == 0) return 12710.f;
	else if (floor_num == 1) return 19700.f;
	else if (floor_num == 2) return 25800.f;
	else return -1;
}

int ASpy::getFloor(FVector location) {
	float locationZ = location.Z;
	if (locationZ > 12710.f && locationZ < 18370.f) return 0;		//Basement - 13710.0 to 18370.0
	else if (locationZ > 19700.f && locationZ < 25800.f) return 1;	//First Floor - 19700.0 to 25800.0
	else if (locationZ > 25800.f) return 2; 					//Roof - 25800.0 upwards
	else return -1;
}


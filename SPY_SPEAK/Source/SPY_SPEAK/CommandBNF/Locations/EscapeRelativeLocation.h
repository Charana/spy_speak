// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RelativeLocation.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API EscapeRelativeLocation : public RelativeLocation 
{
public:
	EscapeRelativeLocation() {};
	virtual Either<AActor*, ActionError> accept(LocationVisitor* visitor) override {
		return visitor->visit(this);
	}

	virtual std::string toString() {
		return "";
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Extras/Either.h"
#include <string>
#include "CommandBNF/Actions/Action.h"
#include "CommandBNF/ActionError.h"
#include "CoreMinimal.h"

class StairsRelativeLocation;
class Stop;
class AbsoluteLocation;
class ObjectRelativeLocation;
class CharacterRelativeLocation;
class EscapeRelativeLocation;


class SPY_SPEAK_API LocationVisitor
{
public:
	std::string name;
	bool isNotMovement = false;
	virtual ~LocationVisitor(){};
	void Init(std::string name) {
		UE_LOG(LogTemp, Warning, TEXT("Init<%s>"), *FString(name.c_str()));
		this->name = name;
	}
	void InitNotMovement(bool isNotMovement) {
		this->isNotMovement = isNotMovement;
	}

	virtual Either<AActor*, ActionError> visit(AbsoluteLocation* location) = 0;
	virtual Either<AActor*, ActionError> visit(ObjectRelativeLocation* location) = 0;
	virtual Either<AActor*, ActionError> visit(CharacterRelativeLocation* location) = 0;
	virtual Either<AActor*, ActionError> visit(StairsRelativeLocation* location) = 0;
	virtual Either<AActor*, ActionError> visit(EscapeRelativeLocation* location) = 0;
};

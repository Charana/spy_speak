// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RelativeLocation.h"
#include "Direction.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API CharacterRelativeLocation : public RelativeLocation
{
public:
	enum class Distance { SHORT, MEDIUM, FAR };
	Direction direction;
	int magnitude;

	CharacterRelativeLocation(Direction direction) : direction(direction), magnitude(1) {};
	CharacterRelativeLocation(Direction direction, int magnitude) : direction(direction), magnitude(magnitude) {};
	virtual Either<AActor*, ActionError> accept(LocationVisitor* visitor) override {
		return visitor->visit(this);
	}


	virtual std::string toString() {
		std::string output("{ Direction - ");
		if (direction == Direction::INFRONT) output.append("Infront, ");
		if (direction == Direction::BEHIND) output.append("Behind, ");
		if (direction == Direction::LEFT) output.append("Left, ");
		if (direction == Direction::BEHIND) output.append("Right, ");

		output.append("Magnitude - " + std::to_string(magnitude) + "}");
		return output;
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RelativeLocation.h"
#include "Direction.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API ObjectRelativeLocation : public RelativeLocation
{
public:
	int index;
	Direction direction;
	ObjectRelativeLocation() : index(0), direction(Direction::NONE) {};
	ObjectRelativeLocation(Direction direction) : index(0), direction(direction) {};
	ObjectRelativeLocation(Direction direction, int index) : index(index), direction(direction) {};
	virtual Either<AActor*, ActionError> accept(LocationVisitor* visitor) override {
		return visitor->visit(this);
	}

	virtual std::string toString() {
		std::string output("{ Direction - ");
		if (direction == Direction::INFRONT) output.append("Infront, ");
		if (direction == Direction::BEHIND) output.append("Behind, ");
		if (direction == Direction::LEFT) output.append("Left, ");
		if (direction == Direction::BEHIND) output.append("Right, ");

		output.append("Index - " + std::to_string(index) + "}");
		return output;
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StairsRelativeLocation.h"
#include "ObjectRelativeLocation.h"
#include "CharacterRelativeLocation.h"
#include "EscapeRelativeLocation.h"
#include "AbsoluteLocation.h"
#include "Character/Spy.h"
#include "Character/SpyController.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LocationVisitorImpl.generated.h"

UCLASS()
class SPY_SPEAK_API ALocationVisitorImpl : public AActor, public LocationVisitor
{
	GENERATED_BODY()
public:
	ALocationVisitorImpl() {};
	TArray<AActor*> VisibleActorsinRadiusofType(ASpy* spy);
	int getFloorNumber(AActor* actor);
	virtual Either<AActor*, ActionError> visit(AbsoluteLocation* location) override;
	virtual Either<AActor*, ActionError> visit(ObjectRelativeLocation* location) override;
	virtual Either<AActor*, ActionError> visit(CharacterRelativeLocation* location) override;
	virtual Either<AActor*, ActionError> visit(StairsRelativeLocation* location) override;
	virtual Either<AActor*, ActionError> visit(EscapeRelativeLocation* location) override;

	ASpy* spyCharacter;

	ASpy* getSpyCharacter() {
		if (IsValid(spyCharacter)) return spyCharacter;
		else {
			TArray<AActor*> ActorsWithTag;
			UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("SpyCharacter"), ActorsWithTag);
			if (ActorsWithTag.IsValidIndex(0)) {
				spyCharacter = Cast<ASpy>(ActorsWithTag[0]);
				return spyCharacter;
			}
			else {
				return nullptr;
			}
		}
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "LocationVisitorImpl.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/GameEngine.h"
#include "Engine/World.h"
#include <algorithm>
#include "Kismet/KismetMathLibrary.h"
#include "Actors/Rotatable.h"
#include "Components/CapsuleComponent.h"
#include "AI/Navigation/NavigationPath.h"
#include "AI/Navigation/NavigationSystem.h"

Either<AActor*, ActionError> ALocationVisitorImpl::visit(AbsoluteLocation* location) {
	TArray<AActor*> outActors, outActors2;
	UE_LOG(LogTemp, Warning, TEXT("ALocationVisitorImpl::ActorName:<%s>"), *FString(name.c_str()));
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName(*FString(name.c_str())), outActors);
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName(*FString(name.c_str())), outActors2);
	outActors.Append(outActors2);
	if (outActors.Num() > 0) {
		UE_LOG(LogTemp, Warning, TEXT("LocationVisitorImpl::AbsoluteLocation - Actor<%s>"), *outActors[0]->GetName());

		ASpy* spyCharacter = getSpyCharacter();
		if (spyCharacter != nullptr) {
			FVector spyLocation = spyCharacter->GetActorLocation();
			outActors.Sort([&spyLocation](const AActor& actor1, const AActor& actor2) {
				return (actor1.GetActorLocation() - spyLocation).Size2D() < (actor2.GetActorLocation() - spyLocation).Size2D();
			});
			return outActors[0];
		}
		else {
			return Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, name));
		}
	}
	else {
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("LocationVisitorImpl::AbsoluteLocation - No actor of that name"));
		return Right<ActionError>(ActionError(ActionError::ActionErrorCode::INVALID_LOCATION, name));
	}
}

bool DirectionBetweenPlayerAndObjectisinDirection(AActor* pawn, AActor* object, Direction direction) { //v1 is forward vector of spy, v2 vector towards object relative to spy
	FVector actorForwardVector = pawn->GetActorForwardVector();
	FVector actorForwardVectorNormalized2D = actorForwardVector.GetSafeNormal2D();
	FVector fraceDirectionNormalized = object->GetActorLocation() - pawn->GetActorLocation();
	FVector faceDirectionNormalized2D = fraceDirectionNormalized.GetSafeNormal2D();
	float degA = std::atan2(actorForwardVectorNormalized2D.Y, actorForwardVectorNormalized2D.X) * 180 / PI;
	float degB = std::atan2(faceDirectionNormalized2D.Y, faceDirectionNormalized2D.X) * 180 / PI;
	float yaw = degB - degA;

	if (direction == Direction::INFRONT && (yaw >= -90 && yaw <= 90)) return true;
	if (direction == Direction::LEFT && (yaw >= -180 && yaw <= 0)) return true;
	if (direction == Direction::BEHIND && (yaw >= 90 && yaw <= -90)) return true;
	if (direction == Direction::RIGHT && (yaw >= 0 && yaw <= 180)) return true;
	else return false;
}

int ALocationVisitorImpl::getFloorNumber(AActor* actor) {
	FVector actorLocation = actor->GetActorLocation();
	int floor_num = -1;
	//FVector floorCenter = actorLocation;
	//float floorHeight = -1;
	//int padding = 1000;
	if (actorLocation.Z > 13710.f && actorLocation.Z < 18370.f) { 	//Basement - 12710.0 to 18370.0
		floor_num = 0;
		//floorCenterLoc.Z = (18370 - 12710) / 2 + 12710 - padding / 2;
		//floorHeight = (18370 - 12710) + padding;
	}
	else if (actorLocation.Z > 19700.f && actorLocation.Z < 25800.f) { //First Floor - 19700.0 to 25800.0
		floor_num = 1;
		//floorCenterLoc.Z = (25800 - 19700) / 2 + 19700 - padding / 2;
		//floorHeight = (25800 - 19700) + padding;
	}
	else if (actorLocation.Z > 25800.f) { 	//Roof - 25800.0 upwards
		floor_num = 2;
		//floorCenterLoc.Z = 5000 / 2 + 25800 - padding / 2;
		//floorHeight = 5000 + padding;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Actor Exists On Invalid Floor Number"));
	}
	return floor_num;
}

TArray<AActor*> ALocationVisitorImpl::VisibleActorsinRadiusofType(ASpy* spy) {

	int spy_floor_num = getFloorNumber(spy);
	TArray<FOverlapResult> overlappingResults;
	UE_LOG(LogTemp, Warning, TEXT("SpyFloorNum[%d]"), spy_floor_num);
	//UE_LOG(LogTemp, Warning, TEXT("floorCenter[%f]"), floorCenterLoc.Z);
	//UE_LOG(LogTemp, Warning, TEXT("floorHeight[%f]"), floorHeight);
	GetWorld()->OverlapMultiByObjectType(overlappingResults, spyCharacter->GetActorLocation(), FQuat(), FCollisionObjectQueryParams::AllObjects, FCollisionShape::MakeSphere(40000));

	//Check if the location exists on map
	TArray<AActor*> outActors;
	TArray<AActor*> navigatablePaths;
	UNavigationSystem* navSys = GetWorld()->GetNavigationSystem();
	for (FOverlapResult& overlapResult : overlappingResults) {
		AActor* actor = overlapResult.GetActor();
		if (actor != NULL && getFloorNumber(actor) == spy_floor_num) {
			//FHitResult outHit;
			//GetWorld()->LineTraceSingleByChannel(outHit, spyCharacter->GetActorLocation(), actor->GetActorLocation(), ECollisionChannel::ECC_Visibility);
			//if(!outHit.IsValidBlockingHit()) navigatablePaths.Add(actor);
			//else if (outHit.IsValidBlockingHit()) {
			//	if(outHit.GetActor() == nullptr || outHit.GetActor() == actor) navigatablePaths.Add(actor);
			//	//else {
			//	//	if (outHit.GetActor() == nullptr) {
			//	//		UE_LOG(LogTemp, Warning, TEXT("Actor[%s]-BlockingActor[%s]"), *actor->GetName(), *FString("wall"));
			//	//	}
			//	//	else {
			//	//		UE_LOG(LogTemp, Warning, TEXT("Actor[%s]-BlockingActor[%s]"), *actor->GetName(), *outHit.GetActor()->GetName());
			//	//	}
			//	//}
			//}
			UNavigationPath* navPath = navSys->FindPathToLocationSynchronously(GetWorld(), spy->GetActorLocation(), actor->GetActorLocation());
			if (navPath == nullptr) continue;
			if (navPath->IsPartial()){
				if ((actor->GetActorLocation() - navPath->PathPoints[navPath->PathPoints.Num() -1]).Size() < 1500) navigatablePaths.Add(actor);
			}
			else navigatablePaths.Add(actor);
		}
	}
	outActors.Empty();
	outActors.Append(navigatablePaths);

	TArray<AActor*> overlappingActorsofType;
	for (AActor* visibleActor : outActors) {
		if (visibleActor->ActorHasTag(FName(*FString(name.c_str())))) overlappingActorsofType.Add(visibleActor);
		UE_LOG(LogTemp, Warning, TEXT("Actors within scope, Actor: %s"), *visibleActor->GetName());
	}
	outActors.Empty();
	outActors.Append(overlappingActorsofType);
	return outActors;
}


Either<AActor*, ActionError> ALocationVisitorImpl::visit(EscapeRelativeLocation* location) {
	ASpy* spyCharacter = getSpyCharacter();
	if (spyCharacter == nullptr) return Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, ""));

	TArray<AActor*> outActors = VisibleActorsinRadiusofType(spyCharacter);
	if (outActors.IsValidIndex(0)) {
		AEnemy* guard = Cast<AEnemy>(outActors[0]);
		FVector escapeLocation = spyCharacter->GetActorLocation() - (guard->GetActorLocation() - spyCharacter->GetActorLocation()).GetSafeNormal2D() * 6000;
		AActor* escapeActor = GetWorld()->SpawnActor<ARotatable>(escapeLocation, FRotator::ZeroRotator);
		return escapeActor;
	}
	else {
		return Right<ActionError>(ActionError(ActionError::ActionErrorCode::NO_GUARDS_IN_SIGHT, name));
	}
}


Either<AActor*, ActionError> ALocationVisitorImpl::visit(ObjectRelativeLocation* location) {
	ASpy* spyCharacter = getSpyCharacter();
	if (spyCharacter == nullptr) return Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, ""));
	FVector spysLocation = spyCharacter->GetActorLocation();

	TArray<AActor*> overlappingActorsofType, outActors;
	overlappingActorsofType = VisibleActorsinRadiusofType(spyCharacter);
	outActors.Append(overlappingActorsofType);

	if (location->direction != Direction::NONE) {
		TArray<AActor*> overlappingActorsofTypeinDirection;
		for (AActor* actor : overlappingActorsofType) {
			if(DirectionBetweenPlayerAndObjectisinDirection(spyCharacter, actor, location->direction)) overlappingActorsofTypeinDirection.Add(actor);
		}
		outActors.Empty();
		outActors.Append(overlappingActorsofTypeinDirection);
	}

	TArray<AActor*> overlappingActorsofTypeinDirectionbyOrder(outActors);
	overlappingActorsofTypeinDirectionbyOrder.Sort([&spysLocation](const AActor& actor1, const AActor& actor2) {
		return (actor1.GetActorLocation() - spysLocation).Size2D() < (actor2.GetActorLocation() - spysLocation).Size2D();
	});
	if (overlappingActorsofTypeinDirectionbyOrder.IsValidIndex(location->index)) {
		UE_LOG(LogTemp, Warning, TEXT("LocationVisitorImpl::RelativeLocation - Actor<%s>"), *overlappingActorsofTypeinDirectionbyOrder[location->index]->GetName());
		return overlappingActorsofTypeinDirectionbyOrder[location->index];
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("LocationVisitorImpl::RelativeLocation - No actor of that name"));
		return Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, name));
	}
}


#include "Components/SkeletalMeshComponent.h"
#include "AI/Navigation/NavigationSystem.h"
#include "AI/Navigation/NavigationPath.h"
#include "Engine/EngineTypes.h"
Either<AActor*, ActionError> ALocationVisitorImpl::visit(CharacterRelativeLocation* location)
{
	ASpy* spy = getSpyCharacter();
	if (spy == nullptr) return Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, ""));

	FVector directionOfMovement;
	USkeletalMeshComponent* skeletol_mesh = spy->GetMesh();
	if (location->direction == Direction::INFRONT) directionOfMovement = spy->GetActorForwardVector();
	if (location->direction == Direction::BEHIND) directionOfMovement = -1 * spy->GetActorForwardVector();
	if (location->direction == Direction::RIGHT) directionOfMovement = spy->GetActorRightVector();
	if (location->direction == Direction::LEFT) directionOfMovement = -1 * spy->GetActorRightVector();
	//directionOfMovement = skeletol_mesh->GetComponentRotation().RotateVector(directionOfMovement);

	FVector spyStartLocation = spy->GetActorLocation();
	float stepSize = 1250;
	FVector spyDestLocation = spyStartLocation + directionOfMovement * location->magnitude * stepSize;
	UE_LOG(LogTemp, Warning, TEXT("Vector: %s"), *spyStartLocation.ToString());
	UE_LOG(LogTemp, Warning, TEXT("Vector: %s"), *spyDestLocation.ToString());
	//UE_LOG(LogTemp, Warning, TEXT("Rotator: %s"), *spyController->GetControlRotation().ToString());
	//UE_LOG(LogTemp, Warning, TEXT("Rotator: %s"), *skeletol_mesh->GetComponentRotation().ToString());

	if (!isNotMovement) {
		//Check if location can be reached (there is no blocking actor (object/wall) inbetween)
		FHitResult outHit;
		GetWorld()->LineTraceSingleByChannel(outHit, spyStartLocation, spyDestLocation, ECollisionChannel::ECC_Visibility);
		if (outHit.IsValidBlockingHit()) {
			spyDestLocation = outHit.Location;
			if (outHit.GetActor() == nullptr) {
				if ((outHit.Location - spyStartLocation).Size2D() < 4000.0f) {
					return Right<ActionError>(ActionError(ActionError::ActionErrorCode::BLOCKING_OBJECT, "wall"));
				}
			}
			else {
				AActor* blockingActor = outHit.GetActor();
				FString objectName = blockingActor->Tags.IsValidIndex(0) ? blockingActor->Tags[0].ToString() : blockingActor->GetName();
				UE_LOG(LogTemp, Warning, TEXT("distanceToImpactPoint[%f]"), ((outHit.Location - spyStartLocation).Size2D()));
				if ((outHit.Location - spyStartLocation).Size2D() < 4000.0f) {
					return Right<ActionError>(ActionError(ActionError::ActionErrorCode::BLOCKING_OBJECT, std::string(TCHAR_TO_UTF8(*objectName))));
				}
			}
		}
	}

	AActor* dest = GetWorld()->SpawnActor<ARotatable>(spyDestLocation, FRotator::ZeroRotator);
	dest->SetActorHiddenInGame(true);
	return dest;
}
//
//Either<AActor*, ActionError> ALocationVisitorImpl::visit(StairsRelativeLocation* stairs)
//{
//	TArray<AActor*> outActors;
//	ASpy* spyCharacter = getSpyCharacter();
//	if (spyCharacter != nullptr) {
//		outActors = VisibleActorsinRadiusofType(spyCharacter);
//		if (outActors.IsValidIndex(0)) return outActors[0];
//		else return Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "stairs"));
//	}
//	else {
//		return Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, ""));
//	}
//}

#include "Actors/Stairs.h"
Either<AActor*, ActionError> ALocationVisitorImpl::visit(StairsRelativeLocation* stairsRelativeMovement)
{
	Either<AActor*, ActionError> result = visit(new ObjectRelativeLocation());
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		AStairs* stairsObject = Cast<AStairs>(destActor);

		ASpy* spyCharacter = getSpyCharacter();
		if (IsValid(spyCharacter)) {
			FString direction = stairsRelativeMovement->direction;
			FString floorString = spyCharacter->floorIntToString(spyCharacter->getCurrentFloor());
			AActor** floorActor = stairsObject->directionToActor.Find(direction + floorString);
			if (floorActor == nullptr) {
				return Right<ActionError>(ActionError(ActionError::ActionErrorCode::INVALID_STAIRS_MOVEMENT, ""));
			}
			return *floorActor;
		}
		else {
			return Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, ""));
		}
	}
	else return result.getRight();
}



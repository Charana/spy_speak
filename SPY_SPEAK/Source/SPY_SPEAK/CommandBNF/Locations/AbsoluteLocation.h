// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandBNF/ActionError.h"
#include "Location.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API AbsoluteLocation : public Location
{
public:
	virtual Either<AActor*, ActionError> accept(LocationVisitor* visitor) override {
		return visitor->visit(this);
	}
	virtual std::string toString() override {
		return std::string("Absolute Location");
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandBNF/ActionError.h"
#include "LocationVisitor.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API Location
{
public:
	virtual Either<AActor*, ActionError> accept(LocationVisitor* visitor) = 0;
	virtual std::string toString() = 0;
};

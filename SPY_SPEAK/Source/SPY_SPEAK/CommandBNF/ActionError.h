// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <string>
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API ActionError
{
public:
	const enum class ActionErrorCode { 
		GAME_ERROR = 0, 
		INVALID_LOCATION = 1, 
		ACTION_IN_COMPOSITE_FAILED = 2,
		INVALID_STAIRS_MOVEMENT = 3, 
		CANNOT_SEE_OBJECT = 4,
		OUTSIDE_MAP = 5, 
		BLOCKING_OBJECT = 6, 
		SPY_NOT_HOLDING_OBJECT = 7,
		OBJECT_NOT_PICKUPABLE = 8,
		GUARD_ALERTED = 9,
		NO_GUARDS_IN_SIGHT = 10
	};

	ActionErrorCode errorCode;
	std::string subject;
	ActionError(ActionErrorCode errorCode, std::string subject) : errorCode(errorCode), subject(subject) {};
	ActionError(ActionErrorCode errorCode) : ActionError(errorCode, "") {};
};

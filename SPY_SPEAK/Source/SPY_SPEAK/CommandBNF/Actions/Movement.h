// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandBNF/GameObjects/GameObject.h"
#include "Speed.h"
#include "Stance.h"
#include "Action.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API Movement : public Action
{
public:
	GameObject gameObject;
	Speed speed;
	Stance stance; 
	Movement(GameObject gameObject, Speed speed, Stance stance, bool isRequestedAction) 
		: gameObject(gameObject), speed(speed), stance(stance), Action(Action::ActionType::MOVEMENT, isRequestedAction){};
	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		std::string output("Action - move, ");
		output.append("{ Speed - ");
		if (speed == Speed::RUN) output.append("Fast, ");
		if (speed == Speed::WALK) output.append("Walk, ");

		output.append("Stance - ");
		if (stance == Stance::CROUCH) output.append("Crouch, ");
		if (stance == Stance::STAND) output.append("Stance, ");
		if (stance == Stance::NO_CHANGE) output.append("NO_CHANGE, ");

		output.append(gameObject.toString());
		output.append(" }");
		return output;
	}
};
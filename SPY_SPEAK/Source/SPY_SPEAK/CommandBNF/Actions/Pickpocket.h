// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandBNF/GameObjects/GameObject.h"
#include "Action.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API Pickpocket : public Action
{
public:
public:
	GameObject gameObject;
	Pickpocket(GameObject gameObject, bool isRequestedAction) : gameObject(gameObject), Action(Action::ActionType::PICKPOCKET, isRequestedAction) {}
	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		return "";
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Action.h"
#include "CommandBNF/GameObjects/GameObject.h"
#include "Actors/Pickupable.h"

class SPY_SPEAK_API Distract : public Action
{
public:
	enum class DistractDirection { TOWARDS_YOU, AWAY_FROM_YOU, LEFT_OF_YOU, RIGHT_OF_YOU };

	DistractDirection distractDirection;
	GameObject gameObject;
	Distract(DistractDirection distractDirection, GameObject gameObject, bool isRequestedAction)
		: distractDirection(distractDirection), gameObject(gameObject), Action(Action::ActionType::DISTRACT, isRequestedAction) {};

	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		return "";
	}
};

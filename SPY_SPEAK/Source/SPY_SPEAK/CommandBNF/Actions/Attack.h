// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Action.h"
#include "CommandBNF/GameObjects/GameObject.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API Attack : public Action
{
public:
	GameObject gameObject;
	Attack(GameObject gameObject, bool isRequestedAction) : gameObject(gameObject), Action(ActionType::ATTACK, isRequestedAction) {};

	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		return "";
	}
};

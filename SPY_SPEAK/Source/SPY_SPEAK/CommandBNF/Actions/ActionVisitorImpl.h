// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionHandler.h"
#include "Extras/Either.h"
#include "ChangeStance.h"
#include "Movement.h"
#include "OpenDoor.h"
#include "Pickup.h"
#include "Stop.h"
#include "Throw.h"
#include "HackCamera.h"
#include "Knockout.h"
#include "Drop.h"
#include "Fight.h"
#include "Strangle.h"
#include "Pickpocket.h"
#include "HackTerminal.h"
#include "Distract.h"
#include "CompositeAction.h"
#include "Hide.h"
#include "Turn.h"
#include "Attack.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Character/SpyController.h"
#include "Extras/Data.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "UObject/ConstructorHelpers.h"
#include "AI/Navigation/RecastNavMesh.h"
#include "ActionVisitorImpl.generated.h"

UCLASS()
class SPY_SPEAK_API AActionVisitorImpl : public AActor, public ActionVisitor
{
	GENERATED_BODY()
public:
	AActionHandler* actionHandler;
	AActionVisitorImpl();

	void Init(AActionHandler* actionHandler) {
		this->actionHandler = actionHandler;
	}

	virtual void visit(Movement* move, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Stop* stop, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(OpenDoor* openDoor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Pickup* pickup, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Throw* throwable, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(ChangeStance* changeStance, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(CompositeAction* composite, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Hide* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(HackCamera* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(HackTerminal* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Turn* turn, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Knockout* knockout, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Pickpocket* pickpocket, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Drop* drop, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Strangle* strangle , std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Attack* attack, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Distract* distract, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(Fight* fight, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	virtual void visit(SnapRotation* snap_rotation, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override;
	void eqsQueryFinished(TSharedPtr<FEnvQueryResult> Result);
	Data* calcThrowData(ASpy* spyCharacter, AActor* destActor);
	


protected:
	Action* action;
	std::function<void(Action*, Either<CompositeData*, ActionError>)> callback;
	UEnvQuery* hideEQSQuery;
	ARecastNavMesh* navData;

	//Spy Character
	ASpy* spyCharacter;
	ASpy* getSpyCharacter();

};

//Make the AI, test by pressing a key and going to the next location on the map based on tags,
//Make a method that takes a location, and movements AI to that location
//Testing would be calling the PlayerController from The ActionHandler

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Action.h"
#include "CommandBNF/GameObjects/GameObject.h"
#include "Actors/Pickupable.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API Knockout : public Action
{
public:
	GameObject gameObject;
	Knockout(GameObject gameObject, bool isRequestedAction) : gameObject(gameObject), Action(Action::ActionType::KNOCKOUT, isRequestedAction) {};

	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		return "";
	}
};

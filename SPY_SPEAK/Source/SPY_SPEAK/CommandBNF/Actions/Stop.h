// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Action.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API Stop : public Action
{
public:
	Stop(bool isRequestedAction) : Action(Action::ActionType::STOP, isRequestedAction){};
	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		return "";
	}
};

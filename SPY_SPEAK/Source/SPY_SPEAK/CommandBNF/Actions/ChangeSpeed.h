// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Action.h"
#include "Speed.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API ChangeSpeed : public Action
{
public:
	Speed speed;
	ChangeSpeed(Speed speed, bool isRequestedAction) : speed(speed), Action(Action::ActionType::CHANGE_SPEED, isRequestedAction) {};

	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		return;
	}

	virtual std::string toString() override {
		return "";
	}
};

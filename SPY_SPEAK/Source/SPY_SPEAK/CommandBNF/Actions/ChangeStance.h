// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Stance.h"
#include "Action.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API ChangeStance : public Action
{
public:
	Stance stance;
	ChangeStance(Stance stance, bool isRequestedAction) : stance(stance), Action(Action::ActionType::CHANGE_STANCE, isRequestedAction) {};
	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}
	virtual std::string toString() override {
		return "";
	}
};

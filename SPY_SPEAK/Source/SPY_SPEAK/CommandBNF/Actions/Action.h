// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <functional>
#include "Extras/Either.h"
#include "ActionVisitor.h"
#include "CommandBNF/ActionError.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API Action
{
public:
	enum class ActionType { KNOCKOUT, CHANGE_STANCE, CHANGE_SPEED, COMPOSITE, MOVEMENT, OPEN_DOOR, 
		PICKUP, STOP, THROW, TURN, HIDE, HACK_TERMINAL, HACK_CAMERA, PICKPOCKET, DROP,
		STRANGLE, DISTRACT, FIGHT, ATTACK, SNAP_ROTATION };

	bool isRequestedAction;
	ActionType type;

	virtual ~Action() {};
	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual std::string toString() = 0;

protected:
	Action() {};
	Action(ActionType type, bool isRequestedAction) : type(type), isRequestedAction(isRequestedAction) {};
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "ActionVisitorImpl.h"
#include "Actors/Door.h"
#include "CommandBNF/Actions/Stop.h"
#include "CommandBNF/GameObjects/GameObject.h"
#include "Extras/StopData.h"
#include "Extras/PickupData.h"
#include "Extras/OpenData.h"
#include "Extras/MoveData.h"
#include "Extras/ThrowData.h"
#include "Extras/HideData.h"
#include "Extras/StairsData.h"
#include "Extras/PickpocketData.h"
#include "Extras/DropData.h"
#include "Extras/StrangleData.h"
#include "Extras/TurnData.h"
#include "Extras/ChangeStanceData.h"
#include "Extras/RelativeTransformData.h"
#include "Extras/StopGuardMovementData.h"
#include "Extras/TransformData.h"
#include "Extras/CompositeData.h"
#include "Extras/HackCameraData.h"
#include "Extras/HackTerminalData.h"
#include "Character/Enemy.h"
#include "CommandBNF/Locations/LocationVisitorImpl.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"


AActionVisitorImpl::AActionVisitorImpl() {
	//ConstructorHelpers::FClassFinder<UObject> HideEQSQueryClass(TEXT("Blueprint'/Game/Charana/BPs/Spy_BP.Spy_BP'"));
	ConstructorHelpers::FObjectFinder<UEnvQuery> hideEQSQuery(TEXT("/Game/Charana/BPs/EQS/HideEQSQuery"));
	if (hideEQSQuery.Object != nullptr) {
		this->hideEQSQuery = hideEQSQuery.Object;
		UE_LOG(LogTemp, Warning, TEXT("HIDE EQS QUERY FOUND"));
	}
}

void AActionVisitorImpl::visit(Movement* move, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(move->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());

	Either<AActor*, ActionError> result = move->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		Data* stanceData = new ChangeStanceData(move->stance);
		Data* moveData = new MoveData(destActor->GetActorLocation(), move->speed);
		CompositeData* compositeData = new CompositeData({ stanceData, moveData });
		callback(move, Left<CompositeData*>(compositeData));
		return;
	}
	else callback(move, Right<ActionError>(result.getRight()));
}

//void AActionVisitorImpl::visit(CompositeAction* composite) {
//	CompositeData* output = new CompositeData({});
//	for (Action* action : composite->actions) {
//		Either<CompositeData*, std::string> actionCommand = action->accept(this);
//		if (actionCommand.getType() == Type::LEFT) output->addAll(actionCommand.getLeft());
//		if (actionCommand.getType() == Type::RIGHT) {
//			callback(composite, Right<std::string>("Action in composite action failed to parse"));
//			return;
//		}
//	}
//	callback(composite, Left<CompositeData*>(output));
//}

void AActionVisitorImpl::visit(CompositeAction* composite, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {
	CompositeData* output = new CompositeData({});
	std::string* err = nullptr;
	std::function<void(Action*, Either<CompositeData*, ActionError>)> compositeCallback = [&](Action* action, Either<CompositeData*, ActionError> result) {
		if (result.getType() == Type::LEFT) output->addAll(result.getLeft());
		if (result.getType() == Type::RIGHT) err = new std::string("Action in composite action failed to parse");
	};
	for (Action* action : composite->actions) {
		action->accept(this, compositeCallback);
		if (err != nullptr) {
			callback(composite, Right<ActionError>(ActionError(ActionError::ActionErrorCode::ACTION_IN_COMPOSITE_FAILED, "" )));
			return;
		}
	}
	callback(composite, Left<CompositeData*>(output));
}

void AActionVisitorImpl::visit(Stop* stop, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	Data* stopData = new StopData();
	CompositeData* compositeData = new CompositeData({ stopData });
	callback(stop, Left<CompositeData*>(compositeData));
}

void AActionVisitorImpl::visit(HackCamera* hack_camera, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	Data* hackCameraData = new HackCameraData();
	CompositeData* compositeData = new CompositeData({ hackCameraData });
	callback(hack_camera, Left<CompositeData*>(compositeData));
}

#include "Extras/TransformData.h"
void AActionVisitorImpl::visit(HackTerminal* hack_terminal, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(hack_terminal->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());

	Either<AActor*, ActionError> result = hack_terminal->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		ATerminal* terminalActor = Cast<ATerminal>(result.getLeft());
		UArrowComponent* arrowComponent = terminalActor->arrowComponent;
		Data* moveData = new MoveData(arrowComponent->GetComponentLocation(), Speed::WALK);
		Data* fitArrowTransformData = new TransformData(new FVector(arrowComponent->GetComponentLocation()), new FRotator(arrowComponent->GetComponentRotation()));
		Data* hackTerminalData = new HackTerminalData(terminalActor);
		CompositeData* compositeData = new CompositeData({ moveData, fitArrowTransformData, hackTerminalData });
		callback(hack_terminal, Left<CompositeData*>(compositeData));
	}
	else callback(hack_terminal, Right<ActionError>(result.getRight()));
}

void AActionVisitorImpl::visit(OpenDoor* openDoor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(openDoor->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());

	Either<AActor*, ActionError> result = openDoor->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		ADoor* door = Cast<ADoor>(destActor);
		if (door == NULL) {
			callback(openDoor, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "door")));
			return;
		}
		else {
			ASpy* spyCharacter = getSpyCharacter();
			if (spyCharacter) {
				UArrowComponent* closestArrow;
				OpenData::OpenDirection openDirection;
				if ((door->inArrow->GetComponentLocation() - spyCharacter->GetActorLocation()).Size2D()
					< (door->outArrow->GetComponentLocation() - spyCharacter->GetActorLocation()).Size2D()) {
					closestArrow = door->inArrow;
					openDirection = OpenData::OpenDirection::INWARD;
				}
				else { 
					closestArrow = door->outArrow;
					openDirection = OpenData::OpenDirection::OUTWARD;
				}

				Data* moveToStart = new MoveData(closestArrow->GetComponentLocation(), Speed::WALK);
				Data* fitArrowTransformData = new TransformData(new FVector(closestArrow->GetComponentLocation()), new FRotator(closestArrow->GetComponentRotation()));
				Data* openDoorData = new OpenData(door, openDirection);
				CompositeData* compositeData = new CompositeData({ moveToStart, fitArrowTransformData, openDoorData });
				//CompositeData* compositeData = new CompositeData({ openDoorData });
				callback(openDoor, Left<CompositeData*>(compositeData));
				return;
			}
			else {
				callback(openDoor, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
				return;
			}
		}
	}
	else callback(openDoor, Right<ActionError>(result.getRight()));
}

void AActionVisitorImpl::visit(Hide* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {
	//ConstructorHelpers::FObjectFinder<UEnvQuery> hideEQSQuery(TEXT("/Game/Charana/BPs/EQS/HideEQSQuery"));
	if (hideEQSQuery != nullptr) {
		FEnvQueryRequest* queryRequest = new FEnvQueryRequest(hideEQSQuery, this);

		this->action = hide;
		this->callback = callback;
		UE_LOG(LogTemp, Warning, TEXT("hide EQS Request starting"));
		queryRequest->Execute(EEnvQueryRunMode::RandomBest5Pct, this, &AActionVisitorImpl::eqsQueryFinished);
	}
}

#include "Cameras/BodyCamera.h"
void AActionVisitorImpl::visit(Distract* distract, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {
	
	ASpy* spyCharacter = getSpyCharacter();
	if (!IsValid(spyCharacter)) {
		callback(distract, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
		return;
	}

	ASpyController* spyController = Cast<ASpyController>(spyCharacter->GetController());
	if (spyController->currentItem.set == true) {
		ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
		visitor->Init(distract->gameObject.name);
		UGameplayStatics::FinishSpawningActor(visitor, FTransform());

		Either<AActor*, ActionError> result = distract->gameObject.location->accept(visitor);
		if (result.getType() == Type::LEFT) {
			AActor* actor = result.getLeft();
			if (actor->ActorHasTag("enemy")) {
				AEnemy* gaurd = Cast<AEnemy>(actor);

				//Find Distraction Location
				FVector forwardAwayVector = spyCharacter->BodyCam->GetActorForwardVector();
				FVector rightAwayVector = spyCharacter->BodyCam->GetActorRightVector();
				FVector distractionLocation = gaurd->GetActorLocation();
				if (distract->distractDirection == Distract::DistractDirection::AWAY_FROM_YOU) distractionLocation += forwardAwayVector * 10000;
				if (distract->distractDirection == Distract::DistractDirection::TOWARDS_YOU) distractionLocation -= forwardAwayVector * 10000;
				if (distract->distractDirection == Distract::DistractDirection::RIGHT_OF_YOU) distractionLocation += rightAwayVector * 10000;
				if (distract->distractDirection == Distract::DistractDirection::LEFT_OF_YOU) distractionLocation -= rightAwayVector * 10000;

				//Trace to Distraction Location 

				Data* throwData = new ThrowData(distractionLocation, 0.0f);
				CompositeData* compositeData = new CompositeData({ throwData });
				callback(distract, Left<CompositeData*>(compositeData));
			}
			else {
				callback(distract, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard" ))); 
				return;
			}
		}
		else {
			callback(distract, Right<ActionError>(result.getRight()));
			return;
		}
	}
	else {
		callback(distract, Right<ActionError>(ActionError(ActionError::ActionErrorCode::SPY_NOT_HOLDING_OBJECT, "" )));
		return;
	}
}

FText GetVictoryEnumAsString(EEnvQueryStatus::Type EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EEnvQueryStatus"), true);
	if (!EnumPtr) return NSLOCTEXT("Invalid", "Invalid", "Invalid");

	return EnumPtr->GetDisplayNameText(EnumValue);
}

#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
void AActionVisitorImpl::eqsQueryFinished(TSharedPtr<FEnvQueryResult> Result)
{
	UE_LOG(LogTemp, Warning, TEXT("EQS Qeury Finished"));
	EEnvQueryStatus::Type status = Result->GetRawStatus();
	UE_LOG(LogTemp, Warning, TEXT("EQS Result Status - %s"), *GetVictoryEnumAsString(Result->GetRawStatus()).ToString());
	if (status != EEnvQueryStatus::Type::Success) {
		this->callback(this->action, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, "")));
		return;
	}

	TArray<FVector> eqsHidingLocations;
	Result->GetAllAsLocations(eqsHidingLocations);

	ASpy* spyCharacter = getSpyCharacter();
	if (spyCharacter) {
		FVector spyLocation = spyCharacter->GetActorLocation();

		TArray<FOverlapResult> overlappingResults;
		TArray<AActor*> gaurds;
		//GetWorld()->OverlapMultiByChannel(overlappingResults, spyLocation, FQuat(), ECollisionChannel::ECC_Pawn, FCollisionShape::MakeBox(FVector(40000.0f, 40000.0f, 2500.0f)));
		GetWorld()->OverlapMultiByChannel(overlappingResults, spyLocation, FQuat(), ECollisionChannel::ECC_Pawn, FCollisionShape::MakeSphere(40000.0f));
		for (FOverlapResult& result : overlappingResults) {
			AActor* actorInRadius = result.GetActor();
			if (actorInRadius == nullptr) continue;
			if (actorInRadius->ActorHasTag("enemy")) {
				gaurds.Add(actorInRadius);
				UE_LOG(LogTemp, Warning, TEXT("Enemy Detected"));
			}
		}
		if (gaurds.Num() == 0) {
			if (!gaurds.IsValidIndex(0)) {
				this->callback(this->action, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard" )));
				return;
			}
		}
		else {
			for (FVector eqsHidingLocation : eqsHidingLocations) {
				UE_LOG(LogTemp, Warning, TEXT("spyCharacterLocation[%s]"), *spyCharacter->GetActorLocation().ToString());
				UE_LOG(LogTemp, Warning, TEXT("eqsHidingLocation[%s]"), *eqsHidingLocation.ToString());

				//DEBUGGING
				//spyCharacter->SetActorLocation(eqsHidingLocation);

				FHitResult outHit;
				FVector gaurdLocation = gaurds[0]->GetActorLocation();
				GetWorld()->LineTraceSingleByChannel(outHit, eqsHidingLocation, gaurdLocation, ECollisionChannel::ECC_Visibility);
				if (outHit.IsValidBlockingHit()) {
					if (outHit.GetActor() != nullptr && outHit.GetActor()->ActorHasTag("enemy")) {
						this->callback(this->action, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR, "" )));
						return;
					}
					else {
						FVector hidingSpotLocation = outHit.ImpactPoint;
						FVector surfaceNormalAtHidingLocation = outHit.ImpactNormal;
						UE_LOG(LogTemp, Warning, TEXT("ImpactPoint[%s]"), *hidingSpotLocation.ToString());
						UE_LOG(LogTemp, Warning, TEXT("ImpactNormal[%s]"), *surfaceNormalAtHidingLocation.ToString());

						//DEBUGGING
						//spyCharacter->SetActorLocation(hidingSpotLocation);

						Data* changeStance = new ChangeStanceData(Stance::STAND);
						Data* moveData = new MoveData(hidingSpotLocation, Speed::WALK, surfaceNormalAtHidingLocation);
						FVector transformLocation = hidingSpotLocation + surfaceNormalAtHidingLocation * spyCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius();
						transformLocation.Z = spyLocation.Z;
						Data* fitHidingSpotTransform = new TransformData(new FVector(transformLocation), nullptr);


						//DEBUGGING
						//spyCharacter->SetActorLocation(transformLocation);


						FVector2D hideLocToGaurdLoc2D = FVector2D((gaurdLocation - eqsHidingLocation).GetSafeNormal2D());
						FVector2D surfaceNormalAtHidingLocation2D = FVector2D(surfaceNormalAtHidingLocation.GetSafeNormal2D());
						float degA = std::atan2(hideLocToGaurdLoc2D.Y, hideLocToGaurdLoc2D.X) * 180 / PI;
						float degB = std::atan2(surfaceNormalAtHidingLocation2D.Y, surfaceNormalAtHidingLocation2D.X) * 180 / PI;
						float rotationToFaceDirection = degB - degA;
						UE_LOG(LogTemp, Warning, TEXT("ROTATION[%f]"), rotationToFaceDirection);
						if ((rotationToFaceDirection >= 180 && rotationToFaceDirection < 360) || (rotationToFaceDirection <= 0 && rotationToFaceDirection > -180)) {
							Data* hideData = new HideData(HideData::FaceDirection::FACE_LEFT);
							CompositeData* compositeData = new CompositeData({ changeStance, moveData, fitHidingSpotTransform, hideData });
							this->callback(this->action, Left<CompositeData*>(compositeData));
							return;
						}
						if ((rotationToFaceDirection >= 0 && rotationToFaceDirection < 180) || (rotationToFaceDirection <= -180 && rotationToFaceDirection > -360)) {
							Data* hideData = new HideData(HideData::FaceDirection::FACE_RIGHT);
							CompositeData* compositeData = new CompositeData({ changeStance, moveData, fitHidingSpotTransform, hideData });
							this->callback(this->action, Left<CompositeData*>(compositeData));
							return;
						}

						//DEBUGGING
						//this->callback(this->action, Right<std::string>("works"));
						//return;
					}
				}
			}
			this->callback(this->action, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
			return;
		}
	}
	else {
		this->callback(this->action, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
		return;
	}
}

#include "CommandBNF/Actions/SnapRotation.h"
#include "Extras/SnapRotationData.h"
void AActionVisitorImpl::visit(SnapRotation* snap_rotation, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	FRotator delta_rotation;
	switch (snap_rotation->direction) {
	case SnapRotation::Direction::LEFT:
		delta_rotation = FRotator(0, -90.0f, 0);
		break;
	case SnapRotation::Direction::RIGHT:
		delta_rotation = FRotator(0, 90.0f, 0);
		break;
	case SnapRotation::Direction::FORWARDS:
		delta_rotation = FRotator(0, 0.0f, 0);
		break;
	default:
		delta_rotation = FRotator::ZeroRotator;
	}

	Data* snapRotationData = new SnapRotationData(delta_rotation);
	CompositeData* compositeData = new CompositeData({ snapRotationData });
	callback(snap_rotation, Left<CompositeData*>(compositeData));
	return;
}


#include "Math/UnrealMathUtility.h"
#include "Extras/RelativeTransformData.h"
void AActionVisitorImpl::visit(Turn* turn, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	FRotator delta_rotation;
	switch (turn->direction) {
		case Turn::Direction::LEFT:
			delta_rotation = FRotator(0, -90.0f, 0);
			break;
		case Turn::Direction::RIGHT:
			delta_rotation = FRotator(0, 90.0f, 0);
			break;
		case Turn::Direction::FORWARDS:
			delta_rotation = FRotator(0, 0.0f, 0);
			break;
	}

	ASpy* spyCharacter = getSpyCharacter();
	//if(spyCharacter){
	//	Data* relativeTransformData = new RelativeTransformData(FVector::ZeroVector, delta_rotation);
	//	CompositeData* compositeData = new CompositeData({ relativeTransformData });
	//	callback(turn, Left<CompositeData*>(compositeData));
	//	return;
	//}
	//else {
	//	callback(turn, Right<std::string>(std::string("GAME ERROR! (Turn) :: SpyCharacter could not be found")));
	//	return;
	//}

	if(spyCharacter){
		float yaw = round((spyCharacter->GetActorRotation() + delta_rotation).Yaw / 90) * 90;
		FRotator newSpyRotation = spyCharacter->GetActorRotation();
		newSpyRotation.Yaw = yaw;

		Data* turnData = new TurnData(yaw, turn->direction);
		Data* rotateCharacter = new TransformData(nullptr, new FRotator(newSpyRotation));
		CompositeData* compositeData = new CompositeData({ turnData });
		callback(turn, Left<CompositeData*>(compositeData));
		return;
	}
	else {
		callback(turn, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
		return;
	}
}

void AActionVisitorImpl::visit(Pickup* pickup, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(pickup->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());

	Either<AActor*, ActionError> result = pickup->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		if (!destActor->ActorHasTag("pickupable")) {
			callback(pickup, Right<ActionError>(ActionError(ActionError::ActionErrorCode::OBJECT_NOT_PICKUPABLE, "")));
			return;
		}

		APickupable* pickupable = Cast<APickupable>(destActor);
		ASpy* spyCharacter = getSpyCharacter();
		if (spyCharacter) {
			float characterDepth = spyCharacter->GetActorLocation().Z;
			float floorDepth = -1;
			if (characterDepth > 12710.f && characterDepth < 18370.f) floorDepth = 12710.f;		//Basement - 13710.0 to 18370.0
			else if (characterDepth > 19700.f && characterDepth < 25800.f) floorDepth = 19700.f;	//First Floor - 19700.0 to 25800.0
			else if (characterDepth > 25800.f) floorDepth = 25800.f; 					//Roof - 25800.0 upwards
			if (floorDepth == -1) {
				callback(pickup, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
				return;
			}

			float floorToPickup = pickupable->GetActorLocation().Z - floorDepth;
			float floorToCharacter = spyCharacter->GetActorLocation().Z - floorDepth;
			float ratio = floorToPickup / floorToCharacter;
			PickupData::PickupLevel pickupLevel = PickupData::PickupLevel::NONE;
			if (ratio >= 0.5) pickupLevel = PickupData::PickupLevel::WAIST;
			else if (ratio < 0.5) pickupLevel = PickupData::PickupLevel::FLOOR;
			if (pickupLevel == PickupData::PickupLevel::NONE) {
				callback(pickup, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
				return;
			}

			Data* moveData = new MoveData(destActor->GetActorLocation(), Speed::WALK);
			Data* pickupData = new PickupData(pickupable, pickupLevel);
			CompositeData* compositeData = new CompositeData({ moveData, pickupData });
			callback(pickup, Left<CompositeData*>(compositeData));
			return;
		}
		else {
			callback(pickup, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
			return;
		}
	}
	else callback(pickup, Right<ActionError>(result.getRight()));
}

#include "Character/Enemy.h"
#include "Components/CapsuleComponent.h"
void AActionVisitorImpl::visit(Throw* tthrow, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ASpy* spyCharacter = getSpyCharacter();
	if (!IsValid(spyCharacter)) {
		callback(tthrow, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
		return;
	}
	ASpyController* spyController = Cast<ASpyController>(spyCharacter->GetController());

	if (spyController->currentItem.set == true) {
		ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
		visitor->Init(tthrow->gameObject.name);
		visitor->InitNotMovement(true);
		UGameplayStatics::FinishSpawningActor(visitor, FTransform());
		Either<AActor*, ActionError> result = tthrow->gameObject.location->accept(visitor);

		if (result.getType() == Type::LEFT) {
			AActor* destActor = result.getLeft();

			float throwElevation;
			if (destActor->ActorHasTag("enemy")) {
				AEnemy* gaurd = Cast<AEnemy>(destActor);
				throwElevation = gaurd->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
			}
			else throwElevation = 0.0f;
			FVector throwLocation = destActor->GetActorLocation();

			FVector2D actorForwardVectorNormalized2D = FVector2D(spyCharacter->GetActorForwardVector().GetSafeNormal2D());
			FVector2D faceDirectionNormalized2D = FVector2D((throwLocation - spyCharacter->GetActorLocation()).GetSafeNormal2D());
			float degA = std::atan2(actorForwardVectorNormalized2D.Y, actorForwardVectorNormalized2D.X) * 180 / PI;
			float degB = std::atan2(faceDirectionNormalized2D.Y, faceDirectionNormalized2D.X) * 180 / PI;
			float rotationToFaceDirection = degB - degA;

			Data* relativeTransformData = new RelativeTransformData(FVector::ZeroVector, FRotator(0, rotationToFaceDirection, 0));
			Data* throwData = new ThrowData(throwLocation, throwElevation);
			CompositeData* compositeData = new CompositeData({ relativeTransformData, throwData });
			callback(tthrow, Left<CompositeData*>(compositeData));
			return;
		}
		else {
			callback(tthrow, Right<ActionError>(result.getRight()));
			return;
		}
	}
	else {
		callback(tthrow, Right<ActionError>(ActionError(ActionError::ActionErrorCode::SPY_NOT_HOLDING_OBJECT , "")));
		return;
	}

}

#include "BehaviorTree/BlackboardComponent.h"
void AActionVisitorImpl::visit(Attack* attack, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {
	ASpy* spyCharacter = getSpyCharacter();
	if (!IsValid(spyCharacter)) {
		callback(attack, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
		return;
	}
	ASpyController* spyController = Cast<ASpyController>(spyCharacter->GetController());

	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(attack->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());
	Either<AActor*, ActionError> result = attack->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		if (!destActor->ActorHasTag("enemy")) {
			callback(attack, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard")));
			return;
		}
		AEnemy* enemyGuard = Cast<AEnemy>(destActor);
		AAIController* guardController = Cast<AAIController>(enemyGuard->GetController());
		bool isPlayerVisible = guardController->GetBlackboardComponent()->GetValueAsBool(FName("isPlayerVisible"));

		if (isPlayerVisible) {
			this->visit(new Fight(attack->gameObject, true), callback);
		}
		else {
			if (spyController->currentItem.set != true) {
				this->visit(new Strangle(attack->gameObject, true), callback);
			}
			else {
				this->visit(new Knockout(attack->gameObject, true), callback);
			}
		}
	}
	else {
		callback(attack, Right<ActionError>(result.getRight()));
		return;
	}
}


void AActionVisitorImpl::visit(Fight* fight, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {

	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(fight->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());
	Either<AActor*, ActionError> result = fight->gameObject.location->accept(visitor);

	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		if (!destActor->ActorHasTag("enemy")) {
			callback(fight, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard")));
			return;
		}
		AEnemy* enemyGuard = Cast<AEnemy>(destActor);
		Data* changeStance = new ChangeStanceData(Stance::STAND);
		Data* moveData = new MoveData(enemyGuard->GetActorLocation(), Speed::RUN);
		CompositeData* compositeData = new CompositeData({ changeStance, moveData });
		callback(fight, Left<CompositeData*>(compositeData));
	}
	else {	
		callback(fight, Right<ActionError>(result.getRight()));
		return;
	}
}


void AActionVisitorImpl::visit(Knockout* knockout, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ASpy* spyCharacter = getSpyCharacter();
	if (!IsValid(spyCharacter)) {
		callback(knockout, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GAME_ERROR)));
		return;
	}
	ASpyController* spyController = Cast<ASpyController>(spyCharacter->GetController());

	if (spyController->currentItem.set == true) {
		ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
		visitor->Init(knockout->gameObject.name);
		UGameplayStatics::FinishSpawningActor(visitor, FTransform());
		Either<AActor*, ActionError> result = knockout->gameObject.location->accept(visitor);
		if (result.getType() == Type::LEFT) {
			AActor* destActor = result.getLeft();
			if (!destActor->ActorHasTag("enemy")) {
				callback(knockout, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard" )));
				return;
			}
			AEnemy* enemyGuard = Cast<AEnemy>(destActor);
			AAIController* guardController = Cast<AAIController>(enemyGuard->GetController());
			bool isPlayerVisible = guardController->GetBlackboardComponent()->GetValueAsBool(FName("isPlayerVisible"));
			if (isPlayerVisible) {
				callback(knockout, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GUARD_ALERTED, "guard")));
				return;
			}

			FVector throwLocation = enemyGuard->GetActorLocation();
			FVector2D actorForwardVectorNormalized2D = FVector2D(spyCharacter->GetActorForwardVector().GetSafeNormal2D());
			FVector2D faceDirectionNormalized2D = FVector2D((throwLocation - spyCharacter->GetActorLocation()).GetSafeNormal2D());
			float degA = std::atan2(actorForwardVectorNormalized2D.Y, actorForwardVectorNormalized2D.X) * 180 / PI;
			float degB = std::atan2(faceDirectionNormalized2D.Y, faceDirectionNormalized2D.X) * 180 / PI;
			float rotationToFaceDirection = degB - degA;

			Data* relativeTransformData = new RelativeTransformData(FVector::ZeroVector, FRotator(0, rotationToFaceDirection, 0));
			Data* throwData = new ThrowData(throwLocation, enemyGuard->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
			CompositeData* compositeData = new CompositeData({ relativeTransformData, throwData });
			callback(knockout, Left<CompositeData*>(compositeData));
		}
		else {
			callback(knockout, Right<ActionError>(result.getRight()));
			return;
		}
	}
	else {
		callback(knockout, Right<ActionError>(ActionError(ActionError::ActionErrorCode::SPY_NOT_HOLDING_OBJECT, "")));
		return;
	}
}

void AActionVisitorImpl::visit(Pickpocket* pickpocket, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(pickpocket->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());
	Either<AActor*, ActionError> result = pickpocket->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		if (!destActor->ActorHasTag("enemy")) {
			callback(pickpocket, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard" )));
			return;
		}
		AEnemy* enemyGaurd = Cast<AEnemy>(destActor);
		Data* crouchData = new ChangeStanceData(Stance::CROUCH);
		Data* moveData = new MoveData(enemyGaurd->GetActorLocation(), Speed::WALK);
		Data* pickpocketData = new PickpocketData(enemyGaurd);
		CompositeData* compositeData = new CompositeData({ crouchData, moveData, pickpocketData });
		callback(pickpocket, Left<CompositeData*>(compositeData));
	}
	else callback(pickpocket, Right<ActionError>(result.getRight()));
}

void AActionVisitorImpl::visit(Drop* drop, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {
	Data* dropData = new DropData();
	CompositeData* compositeData = new CompositeData({ dropData });
	callback(drop, Left<CompositeData*>(compositeData));
}

void AActionVisitorImpl::visit(Strangle* strangle, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback)
{
	ALocationVisitorImpl* visitor = Cast<ALocationVisitorImpl>(UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), ALocationVisitorImpl::StaticClass(), FTransform()));
	visitor->Init(strangle->gameObject.name);
	UGameplayStatics::FinishSpawningActor(visitor, FTransform());
	Either<AActor*, ActionError> result = strangle->gameObject.location->accept(visitor);
	if (result.getType() == Type::LEFT) {
		AActor* destActor = result.getLeft();
		if (!destActor->ActorHasTag("enemy")) {
			callback(strangle, Right<ActionError>(ActionError(ActionError::ActionErrorCode::CANNOT_SEE_OBJECT, "guard" )));
			return;
		}
		AEnemy* enemyGuard = Cast<AEnemy>(destActor);
		AAIController* guardController = Cast<AAIController>(enemyGuard->GetController());
		bool isDistracted = guardController->GetBlackboardComponent()->GetValueAsBool(FName("isDistracted"));
		bool isPlayerVisible = guardController->GetBlackboardComponent()->GetValueAsBool(FName("isPlayerVisible"));
		bool isCatious = isDistracted || isPlayerVisible;
		if (isCatious) {
			callback(strangle, Right<ActionError>(ActionError(ActionError::ActionErrorCode::GUARD_ALERTED, "guard")));
			return;
		}

		StopGuardMovementData* stopGuardMovementData = new StopGuardMovementData(enemyGuard);
		Data* changeStanceCrouch = new ChangeStanceData(Stance::CROUCH);
		//FVector displacement = enemyGaurd->GetActorForwardVector() * -1 * (8 * enemyGaurd->GetCapsuleComponent()->GetScaledCapsuleRadius())
		//						+ enemyGaurd->GetActorRightVector() * 1 * (-0.25 * enemyGaurd->GetCapsuleComponent()->GetScaledCapsuleRadius());
		FVector forwardDisplacement = enemyGuard->GetActorForwardVector() * -1 * 190 * 17;
		FVector displacement = forwardDisplacement
								+ enemyGuard->GetActorForwardVector() * -1 *  (1.20 * enemyGuard->GetCapsuleComponent()->GetScaledCapsuleRadius())
								+ enemyGuard->GetActorRightVector() * 1 * (-0.25 * enemyGuard->GetCapsuleComponent()->GetScaledCapsuleRadius());
		FVector strangleLoc = enemyGuard->GetActorLocation() + displacement;
		MoveData* moveData = new MoveData(strangleLoc, Speed::WALK, enemyGuard->GetActorForwardVector());
		Data* transformData = new TransformData(new FVector(strangleLoc), nullptr);
		Data* strangleData = new StrangleData(enemyGuard, forwardDisplacement);
		CompositeData* compositeData = new CompositeData({ stopGuardMovementData, changeStanceCrouch, moveData, transformData, strangleData });
		callback(strangle, Left<CompositeData*>(compositeData));
	}
	else callback(strangle, Right<ActionError>(result.getRight()));
}


void AActionVisitorImpl::visit(ChangeStance* changeStance, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) {
	CompositeData* compositeData = new CompositeData({ new ChangeStanceData(changeStance->stance) });
	callback(changeStance, Left<CompositeData*>(compositeData));
}

ASpy* AActionVisitorImpl::getSpyCharacter()
{
	if(IsValid(spyCharacter)){
		return spyCharacter;
	}
	else {
		TArray<AActor*> outActors;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("SpyCharacter"), outActors);
		if (outActors.IsValidIndex(0)) {
			spyCharacter = Cast<ASpy>(outActors[0]);
			return spyCharacter;
		}
		else {
			return nullptr;
		}
	}
}



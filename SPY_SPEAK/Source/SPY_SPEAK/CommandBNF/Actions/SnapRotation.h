// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Action.h"
#include "CoreMinimal.h"

class SPY_SPEAK_API SnapRotation : public Action
{
public:
	enum class Direction { LEFT, RIGHT, FORWARDS, BACKWARDS, NONE };

	Direction direction;
	SnapRotation(Direction direction, bool isRequestedAction) : direction(direction), Action(Action::ActionType::SNAP_ROTATION, isRequestedAction) {};

	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}

	virtual std::string toString() override {
		return "";
	}
};

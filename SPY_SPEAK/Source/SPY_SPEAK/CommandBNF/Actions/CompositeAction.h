// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include "Action.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SPY_SPEAK_API CompositeAction : public Action
{
public:
	std::vector<Action*> actions;
	CompositeAction(std::vector<Action*> actions, bool isRequestedAction) : actions(actions), Action(Action::ActionType::COMPOSITE, isRequestedAction) {};
	virtual void accept(ActionVisitor* visitor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) override {
		visitor->visit(this, callback);
	}
	virtual std::string toString() override {
		return "";
	}
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <functional>
#include <string>
#include "CoreMinimal.h"
#include "CommandBNF/ActionError.h"

class ChangeStance;
class CompositeData;
class OpenDoor;
class Movement;
class Pickup;
class Throw;
class StairsMovement;
class CompositeAction;
class Stop;
class Hide;
class Knockout;
class Strangle;
class Drop;
class Distract;
class Attack;
class SnapRotation;
class HackCamera;
class HackTerminal;
class Pickpocket;
class Fight;
template <typename T, typename U>
class Either;

class Action;
class CompositeData;
class Turn;

class SPY_SPEAK_API ActionVisitor
{
public:
	virtual void visit(Movement* move, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Stop* stop, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(OpenDoor* openDoor, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Pickup* pickup, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Throw* throwable, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(ChangeStance* changeStance, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(CompositeAction* composite, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Hide* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(HackCamera* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(HackTerminal* hide, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Turn* turn, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Knockout* knockout, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Pickpocket* pickpocket, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Drop* drop, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Strangle* strangle, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Attack* attack, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Distract* distract, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(Fight* fight, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
	virtual void visit(SnapRotation* snap_rotation, std::function<void(Action*, Either<CompositeData*, ActionError>)> callback) = 0;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandBNF/Locations/Location.h"
#include "CoreMinimal.h"


class SPY_SPEAK_API GameObject
{
public:
	std::string name;
	Location* location;

	GameObject(Location* location) : name(std::string("Self")), location(location) {};
	GameObject(std::string name, Location* location) : name(name), location(location) {};

	std::string toString() {
		return "{ Name - " + name + ", " + location->toString() + " }";
	}
};

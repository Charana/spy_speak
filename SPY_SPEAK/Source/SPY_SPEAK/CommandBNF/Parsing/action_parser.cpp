//
//  action_parser.cpp
//  EngineCommandSet
//
//  Created by Albie Baker-Smith on 17/11/2017.
//  Copyright © 2017 Albie Baker-Smith. All rights reserved.
//

#include "CommandBNF/Actions/Movement.h"
#include "CommandBNF/Actions/OpenDoor.h"
#include "CommandBNF/Actions/Pickup.h"
#include "CommandBNF/Actions/Throw.h"
#include "CommandBNF/Actions/CompositeAction.h"
#include "CommandBNF/Actions/ChangeStance.h"
#include "CommandBNF/Actions/ChangeSpeed.h"
#include "CommandBNF/Actions/HackCamera.h"
#include "CommandBNF/Actions/HackTerminal.h"
#include "CommandBNF/Actions/Turn.h"
#include "CommandBNF/Actions/Attack.h"
#include "CommandBNF/Actions/Knockout.h"
#include "CommandBNF/Actions/Strangle.h"
#include "CommandBNF/Actions/HackTerminal.h"
#include "CommandBNF/Actions/Drop.h"
#include "CommandBNF/Actions/Hide.h"
#include <vector>
#include <unordered_map>
#include "action_parser.hpp"
#include "object_parser.hpp"
#include "json_helper.hpp"

namespace json_parsing {
	// Parses a Move action, or throws if parsing was unsuccessful.
	Movement *parse_move(json j) throw(exception) {
		auto dest_json = j["dest"];
		auto stanceString = j["stance"];
		auto speedString = j["speed"];

		if (dest_json.is_null() || stanceString.is_null() || speedString.is_null()) throw std::exception("Movement fields not set");
		GameObject *dest = parse_object(dest_json);

		std::unordered_map<std::string, Stance> stringToStance;
		stringToStance["stand"] = Stance::STAND;
		stringToStance["crouch"] = Stance::CROUCH;
		stringToStance["no_change"] = Stance::NO_CHANGE;
		Stance stance = stringToStance[stanceString.get<std::string>()];

		std::unordered_map<std::string, Speed> stringToSpeed;
		stringToSpeed["slow"] = Speed::WALK;
		stringToSpeed["normal"] = Speed::WALK;
		stringToSpeed["fast"] = Speed::RUN;
		Speed speed = stringToSpeed[speedString.get<std::string>()];

		return new Movement(*dest, speed, stance, true);
	}

	OpenDoor* parse_openDoor(json j) throw(exception) {
		auto direction_json = j["direction"];
		if (direction_json.is_null()) throw std::exception("OpenDoor fields not set");

		Direction direction = parse_direction(direction_json);

		OpenDoor* openDoor = new OpenDoor(GameObject("door", new ObjectRelativeLocation(direction)), true);
		return openDoor;
	}

	OpenDoor* parse_leaveRoom(json j) throw(exception) {
		return new OpenDoor(GameObject("leave door", new ObjectRelativeLocation()), true);
	}
	

	Pickup* parse_pickup(json j) throw(exception) {
		auto objectName = j["name"];
		auto directionString = j["direction"];

		if (objectName.is_null() || directionString.is_null()) throw std::exception("Pickup fields not set");

		std::unordered_map<std::string, Direction> stringToDirection;
		stringToDirection["backwards"] = Direction::BEHIND;
		stringToDirection["forwards"] = Direction::INFRONT;
		stringToDirection["left"] = Direction::LEFT;
		stringToDirection["right"] = Direction::RIGHT;
		stringToDirection["vicinity"] = Direction::NONE;
		Direction direction = stringToDirection[directionString.get<std::string>()];
		
		return new Pickup(GameObject(objectName.get<std::string>(), new ObjectRelativeLocation(direction)), true);
	}

	Action* parse_throw(json j) throw(exception) {
		auto locationJSON = j["location"];
		Location* location = parse_location(locationJSON); 
		return new Throw(GameObject(location), true);
	}

	
	Action* parse_composite(json j) throw(exception) {
		std::vector<nlohmann::json> actionsJSON = j["actions"].get<std::vector<nlohmann::json>>();
		std::vector<Action*> actions;
		for (nlohmann::json actionJSON : actionsJSON) {
			Action* action = parse_action(actionJSON);
			actions.push_back(action);
		}
		return new CompositeAction(actions, true);
	}

	Action* parse_changeStance(json j) throw(exception) {
		auto stanceString = j["stance"];

		if (stanceString.is_null()) throw std::exception("Change_Stance fields not set");
		std::unordered_map<std::string, Stance> stringToStance;
		stringToStance["crouch"] = Stance::CROUCH;
		stringToStance["stand"] = Stance::STAND;
		stringToStance["no_change"] = Stance::NO_CHANGE;
		Stance stance = stringToStance[stanceString.get<std::string>()];

		return new ChangeStance(stance, true);
	}

	Action* parse_stop(json j) throw(exception) {
		return new Stop(true);
	}

	Action* parse_hack_camera(json j) throw (exception) {
		return new HackCamera(true);
	}

	Action* parse_hack_terminal(json j) throw (exception) {
		return new HackTerminal(GameObject("Terminal", new ObjectRelativeLocation()), true);
	}

	Action* parse_turn(json j) throw (exception) {
		auto dirJSON = j["direction"];
		if (dirJSON.is_null()) throw MissingJsonFieldParseError("direction", "Turn Action");
		string direction = dirJSON.get<std::string>();

		if (direction == "left") return new Turn(Turn::Direction::LEFT, true);
		else if (direction == "right")  return new Turn(Turn::Direction::RIGHT, true);
		else if (direction == "forward") return new Turn(Turn::Direction::FORWARDS, true);
		else if (direction == "backwards") {
			Action* turn1 = new Turn(Turn::Direction::LEFT, true);
			Action* turn2 = new Turn(Turn::Direction::LEFT, true);
			return new CompositeAction({ turn1, turn2 }, true);
		}
		else return new Turn(Turn::Direction::FORWARDS, true);
	}

	Action* parse_attack(json j) throw(exception) {
		auto directionJSON = j["direction"];
		if (directionJSON.is_null()) throw std::exception("Attack fields not set");

		Direction direction = parse_direction(directionJSON);

		Action* attack = new Attack(GameObject("guard", new ObjectRelativeLocation(direction)), true);
		return attack;
	}

	Action* parse_changeSpeed(json j) throw(exception) {
		auto speed_json = j["speed"];
		if (speed_json.is_null()) throw std::exception("Change Speed fields not set");

		std::unordered_map<std::string, Speed> stringToSpeed;
		stringToSpeed["slow"] = Speed::WALK;
		stringToSpeed["normal"] = Speed::WALK;
		stringToSpeed["fast"] = Speed::RUN;
		Speed speed = stringToSpeed[speed_json.get<std::string>()];

		Action* changeSpeed = new ChangeSpeed(speed, true);
		return changeSpeed;
	}

	Action* parse_strangle(json j) throw(exception) {
		auto direction_json = j["direction"];
		if (direction_json.is_null()) throw std::exception("Strangle fields not set");

		Direction direction = parse_direction(direction_json);

		Action* strangle = new Strangle(GameObject("guard", new ObjectRelativeLocation(direction)), true);
		return strangle;
	}

	
	Action* parse_knockout(json j) throw(exception) {
		auto direction_json = j["direction"];
		if (direction_json.is_null()) throw std::exception("Knockout fields not set");

		Direction direction = parse_direction(direction_json);

		Action* knockout = new Knockout(GameObject("guard", new ObjectRelativeLocation(direction)), true);
		return knockout;
	}

	
	Action* parse_drop(json j) throw(exception) {
		return new Drop(true);
	}

	
	Action* parse_hack(json j) throw(exception) {
		auto direction_json = j["direction"];
		if (direction_json.is_null()) throw std::exception("Hack Terminal fields not set");

		Direction direction = parse_direction(direction_json);
		
		Action* hack_terminal = new HackTerminal(GameObject("terminal", new ObjectRelativeLocation(direction)), true);
		return hack_terminal;
	}

	
	Action* parse_hide(json j) throw(exception) {
		auto object_json = j["direction"];
		if (object_json.is_null()) throw std::exception("Hide fields not set");
		std::string object_name = object_json.get<std::string>();

		Action* hide = new Hide(GameObject(object_name, new ObjectRelativeLocation()), true);
		return hide;
	}


	// Parses an Action, throws if parsing was unsuccessful.
	Action* parse_action(json j) throw(exception) {
		auto type_json = j["type"];
		if (type_json.is_null()) throw MissingJsonFieldParseError("type", "Action");
		string type = type_json.get<string>();
		
		if (type == "move")	return parse_move(j);
		else if (type == "stop") return parse_stop(j);
		else if (type == "leave") return parse_leaveRoom(j);
		else if (type == "hack") return parse_hack(j);
		else if (type == "composite") return parse_composite(j);
		else if (type == "turn") return parse_turn(j);
		else if (type == "opendoor") return parse_openDoor(j);
		else if (type == "pickup") return parse_pickup(j);
		else if (type == "throw") return parse_throw(j);
		else if (type == "drop") return parse_drop(j);
		else if (type == "change_stance") return parse_changeStance(j);
		else if (type == "change_speed") return parse_changeSpeed(j);
		else if (type == "attack") return parse_attack(j);
		else if (type == "strangle") return parse_strangle(j);
		else if (type == "throw_at_guard") return parse_knockout(j);
		else if (type == "hack") return parse_hack(j);
		//else if (type == "distract") return parse_distract();
		else if (type == "hide") return parse_hide(j);
		else return nullptr;
	}


}
//
//  location_parser.hpp
//  EngineCommandSet
//
//  Created by Albie Baker-Smith on 17/11/2017.
//  Copyright © 2017 Albie Baker-Smith. All rights reserved.
//

#ifndef location_parser_hpp
#define location_parser_hpp

#pragma warning( disable : 4290 )
#include "CommandBNF/Locations/Location.h"
#include "CommandBNF/Locations/AbsoluteLocation.h"
#include "CommandBNF/Locations/RelativeLocation.h"
#include "CommandBNF/Locations/ObjectRelativeLocation.h"
#include "CommandBNF/Locations/CharacterRelativeLocation.h"
#include "json_library.hpp"

using nlohmann::json;
using std::exception;

namespace json_parsing {
    // Returns a parsed Location, or an throws if parsing was unsuccessful.
    Location* parse_location(json j) throw(exception);
	Direction parse_direction(json direction_json) throw(exception);
}

#endif /* location_parser_hpp */

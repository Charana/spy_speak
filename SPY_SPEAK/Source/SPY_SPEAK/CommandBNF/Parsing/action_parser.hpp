//
//  action_parser.hpp
//  EngineCommandSet
//
//  Created by Albie Baker-Smith on 17/11/2017.
//  Copyright © 2017 Albie Baker-Smith. All rights reserved.
//

#ifndef action_parser_hpp
#define action_parser_hpp

#pragma warning( disable : 4290 )
#include "json_library.hpp"
#include "CommandBNF/GameObjects/GameObject.h"
#include "CommandBNF/Actions/Action.h"
#include "CommandBNF/Actions/Movement.h"
#include "CommandBNF/Actions/OpenDoor.h"
#include "CommandBNF/Actions/Pickup.h"
#include "CommandBNF/Actions/Throw.h"
#include "CommandBNF/Actions/Stop.h"
#include "CommandBNF/Actions/Speed.h"

using nlohmann::json;
using std::exception;

namespace json_parsing {
    // Parses an Action, throws if parsing was unsuccessful.
    Action *parse_action(json j) throw(exception);
}

#endif /* action_parser_hpp */

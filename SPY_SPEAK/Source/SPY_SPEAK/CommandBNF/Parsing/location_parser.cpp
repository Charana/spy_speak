//
//  location_parser.cpp
//  EngineCommandSet
//
//  Created by Albie Baker-Smith on 17/11/2017.
//  Copyright © 2017 Albie Baker-Smith. All rights reserved.
//

#include "CommandBNF/Locations/StairsRelativeLocation.h"
#include "CommandBNF/Locations/Direction.h"
#include <string>
#include <unordered_map>
#include "location_parser.hpp"
#include "json_helper.hpp"

namespace json_parsing {

    // Returns a parsed Location, or an throws if parsing was unsuccessful.
    Location* parse_absolute_location(json j) throw(exception) {
		return new AbsoluteLocation();
    }

	Location* parse_object_relative_location(json j) throw(exception) {
		auto index = j["index"];
		auto directionString = j["direction"];
		if (index.is_null() || directionString.is_null()) throw std::exception("ObjectRelativeLocation fields not set");

		//Move outside into static initializer
		std::unordered_map<std::string, Direction> stringToDirection;
		stringToDirection["backwards"] = Direction::BEHIND;
		stringToDirection["forwards"] = Direction::INFRONT;
		stringToDirection["left"] = Direction::LEFT;
		stringToDirection["right"] = Direction::RIGHT;
		stringToDirection["vicinity"] = Direction::NONE;

		Direction direction = stringToDirection[directionString];
		//TODO : Check for no correct direction
		return new ObjectRelativeLocation(direction, index.get<int>());
	}


	Location* parse_character_relative_location(json j) throw(exception) { 
		auto amountJSON = j["distance"];
		auto directionJSON = j["direction"];
		if (directionJSON.is_null() || amountJSON.is_null()) throw std::exception("CharacterRelativeLocation fields not set");
		std::string amountString = amountJSON.get<std::string>();
		std::string directionString = directionJSON.get<std::string>();

		
		std::unordered_map<std::string, int> stringToAmount;
		stringToAmount["short"] = 4;
		stringToAmount["medium"] = 8;
		stringToAmount["far"] = 16;

		//Move outside into static initializer
		std::unordered_map<std::string, Direction> stringToDirection;
		stringToDirection["backwards"] = Direction::BEHIND;
		stringToDirection["forwards"] = Direction::INFRONT;
		stringToDirection["left"] = Direction::LEFT;
		stringToDirection["right"] = Direction::RIGHT;
		//Defaults to forward, No need for none
		
		Direction direction = stringToDirection[directionString];
		int amount = stringToAmount[amountString];
		//TODO : Check for no correct diretion
		return new CharacterRelativeLocation(direction, amount);
	}

	Location* parse_stairs_relative_location(json j) throw(exception) {
		auto directionStringJSON = j["direction"];
		if (directionStringJSON.is_null()) throw std::exception("StairsRelativeLocation, direction field not present");
		std::string directionString = directionStringJSON.get<std::string>();

		return new StairsRelativeLocation(FString(directionString.c_str()));
	}

	Location* parse_behind_relative_location(json j) throw(exception) {
		return nullptr; // new BehindRelativeLocation();
	}

	Location* parse_location(json j) throw(exception) {
		auto type = j["type"];
		if (type.is_null()) throw MissingJsonFieldParseError("type", "Location");
		string typeString = type.get<string>();

		if (typeString == "absolute") return parse_absolute_location(j);
		else if (type == "positional") return parse_object_relative_location(j);
		else if (type == "directional") return parse_character_relative_location(j);
		else if (type == "stairs") return parse_stairs_relative_location(j);
		else return nullptr;
	}

	Direction parse_direction(json direction_json) {

		std::string direction = direction_json.get<std::string>();

		std::unordered_map<std::string, Direction> stringToDirection;
		stringToDirection["backwards"] = Direction::BEHIND;
		stringToDirection["forwards"] = Direction::INFRONT;
		stringToDirection["left"] = Direction::LEFT;
		stringToDirection["right"] = Direction::RIGHT;
		stringToDirection["vicinity"] = Direction::NONE;

		Direction direction_enum = stringToDirection[direction];
		return direction_enum;
	}
}

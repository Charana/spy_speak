// Fill out your copyright notice in the Description page of Project Settings.

#include "Camera.h"
#include "Components/InputComponent.h"
#include "Engine/GameEngine.h"
#include "AIController.h"

// Sets default values
ACamera::ACamera()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
	Camera->SetRelativeLocation(FVector(100.0f * 15, 0, 0));
	Camera->SetupAttachment(RootComponent);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName("Pawn");

	AIControllerClass = AAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorld;

	//Tag as Camera
	this->Tags.Add(FName("Camera"));
}

// Called when the game starts or when spawned
void ACamera::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Zoom in if ZoomIn button is down, zoom back out if it's not
	if (bZoomingIn) {
		ZoomFactor += DeltaTime / 0.5f;         //Zoom in over half a second
	}
	else {
		ZoomFactor -= DeltaTime / 0.25f;        //Zoom out over a quarter of a second
	}
	ZoomFactor = FMath::Clamp<float>(ZoomFactor, 0.0f, 1.0f);
	//Blend our camera's FOV and our SpringArm's length based on ZoomFactor
	Camera->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, ZoomFactor);
}

// Called to bind functionality to input
void ACamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	//Hook up events for "ZoomIn"
	PlayerInputComponent->BindAction("ZoomIn", IE_Pressed, this, &ACamera::ZoomIn);
	PlayerInputComponent->BindAction("ZoomIn", IE_Released, this, &ACamera::ZoomOut);											
}

void ACamera::ZoomIn()
{
	bZoomingIn = true;
}

void ACamera::ZoomOut()
{
	bZoomingIn = false;
}